## Replica, a distributed system for the R environment


The repository is divided into two folders:


1. ReplicaLibrary - contains the Replica library, a java class which permits the integration of R programs into Java applications

2. ReplicaWSC - contains the Replica Web Service and Management Conosole, a Java web application which enable the usage of R programs from any programming language with HTTP socket support.

3. ReplicaNA - contains the Replica Node Admin service, a java daemon to be used with the WSC application. Once the Node Admin service is deployed on the server running Rserve process, it can be used to start/stop/reset the Rserve process.


The products are licensed under the GNU General Public License V3, a copy of the license is contained on the source packages.


For further information, please contact Davide Dal farra via the email address dalfarra@codref.com