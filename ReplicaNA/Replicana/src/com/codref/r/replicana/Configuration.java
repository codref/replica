package com.codref.r.replicana;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {
	private static Properties prop = new Properties();
	private static InputStream input = null;
	
	public static void init() {
		try {			 
			input = new FileInputStream("/etc/replicana.conf");
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String get(String key) {
		return prop.getProperty(key);
	}
}
