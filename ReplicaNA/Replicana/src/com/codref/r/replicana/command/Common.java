package com.codref.r.replicana.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Common {
	
	public static boolean findProcess(String commandName, int pid) {
		
		for (HashMap<String,String> process : getProcessStatus(commandName)) {
			if (process.get("pid").equals(Integer.toString(pid)))
				return true;
		}
		
		return false;
	}
	
	public static ArrayList<HashMap<String,String>> getProcessStatus(String commandName) {
		ArrayList<HashMap<String,String>> data = new ArrayList<>();
		String[] cmdOutput = executeCommand(String.format("ps -o pid,pcpu,pmem,start,time,args -C %s", commandName)).split("\\n");
		Pattern regex = Pattern.compile("^\\s*([0-9]+)\\s*([0-9.]+)\\s*([0-9.]+)\\s*([0-9:]+)\\s*([0-9:]+)\\s*(.*)");
		for (String cmdOutputline : cmdOutput) {
			HashMap<String,String> line = new HashMap<>();
			try {
				Matcher regexMatcher = regex.matcher(cmdOutputline);
				while (regexMatcher.find()) {
					line.put("pid", regexMatcher.group(1));
					line.put("pcpu", regexMatcher.group(2));
					line.put("pmem", regexMatcher.group(3));
					line.put("start", regexMatcher.group(4));
					line.put("time", regexMatcher.group(5));
					line.put("args", regexMatcher.group(6));
				}
				if (line.size() > 0) data.add(line);
			} catch (IllegalStateException e) {
				System.out.println("Error");
			}
		}
		return data;
	}
	
	public static String executeCommand(String command) {

		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return output.toString();

	}
}
