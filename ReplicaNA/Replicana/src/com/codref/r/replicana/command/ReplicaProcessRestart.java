package com.codref.r.replicana.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import com.codref.r.replicana.Configuration;

public class ReplicaProcessRestart extends HttpServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1244370089306486688L;


	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			IOException {
			
		try {
			ArrayList<HashMap<String,String>> processes = Common.getProcessStatus("Rserve");
			for (HashMap<String, String> process : processes) {
				String cmdOutput = Common.executeCommand(String.format("kill %d", Integer.parseInt(process.get("pid"))));
			}
			System.out.println(Configuration.get("serve.start.command"));
			String cmdOutput = Common.executeCommand(Configuration.get("rserve.start.command"));
			
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			ObjectMapper mapper = new ObjectMapper();		
			response.getWriter().println(mapper.writeValueAsString("ok"));
			
		} catch (NumberFormatException e) {
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			ObjectMapper mapper = new ObjectMapper();		
			response.getWriter().println(mapper.writeValueAsString("Param error"));
			return;
		}
	}
}
