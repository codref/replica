package com.codref.r.replicana.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

public class ReplicaProcessCount extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2494226546543699787L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			IOException {
		
		
		ArrayList<HashMap<String,String>> data = Common.getProcessStatus("Rserve");
		response.setContentType("text/html");
		response.setStatus(HttpServletResponse.SC_OK);
		ObjectMapper mapper = new ObjectMapper();		
		response.getWriter().println(mapper.writeValueAsString(data.size()));
	}
}
