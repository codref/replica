package com.codref.r.replicana.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

public class ReplicaProcessKill extends HttpServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1244370089306486688L;


	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			IOException {
		int pid = -1;
		try {
			 pid = Integer.parseInt(request.getParameter("pid"));
		} catch (NumberFormatException e) {
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			ObjectMapper mapper = new ObjectMapper();		
			response.getWriter().println(mapper.writeValueAsString("Param error"));
			return;
		}
		if (pid > 0 && Common.findProcess("Rserve", pid)) {
			String cmdOutput = Common.executeCommand(String.format("kill %d", pid));
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			ObjectMapper mapper = new ObjectMapper();		
			response.getWriter().println(mapper.writeValueAsString("ok"));			
		} else {
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			ObjectMapper mapper = new ObjectMapper();		
			response.getWriter().println(mapper.writeValueAsString("Error"));	
		}
		
	}
}
