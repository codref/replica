package com.codref.r.replicana;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;

import com.codref.r.replicana.command.ReplicaProcessCount;
import com.codref.r.replicana.command.ReplicaProcessKill;
import com.codref.r.replicana.command.ReplicaProcessRestart;
import com.codref.r.replicana.command.ReplicaProcessStatus;

public class NodeAdmin {
	public static void main(String[] args) throws Exception {
		Configuration.init();
		Server server = new Server(9967);
		ServletHandler handler = new ServletHandler();
		handler.addServletWithMapping(ReplicaProcessStatus.class,
				"/replicaProcessStatus");
		handler.addServletWithMapping(ReplicaProcessKill.class,
				"/replicaProcessKill");
		handler.addServletWithMapping(ReplicaProcessCount.class,
				"/replicaProcessCount");
		handler.addServletWithMapping(ReplicaProcessRestart.class,
				"/replicaProcessRestart");
		server.setHandler(handler);
		server.start();
		server.join();
	}



}
