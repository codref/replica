/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.view.mainMenu.HeaderMenu', {
	extend : 'Ext.tab.Panel',
	alias : 'widget.headerMenu',
	plain: true,
	border: false,
	autoScroll: true,
	style: {
		padding: '0 0 0 0'
	},
	bodyBorder: false,
	items : [{
		xtype: 'panel',
		border : false,
		iconCls : 'icon-home',
		id: 'home-header-menu',
		action: 'informationsettings',
		items: [],
		onOpen: function() {
			Ext.ComponentQuery.query('#is-grouptabpanel')[0].items.items[0].setWidth(220);
			Ext.ComponentQuery.query('#is-grouptabpanel')[0].setActiveTab(1);
		}
	}, {
		xtype: 'panel',
		border : false,
		title: 'Terminal',
		layout: 'column',
		id: 'spec-header-menu',
		action: 'terminal',
        items: []
	}]
});
