/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.view.informationSettings.InformationSettingsTabsView' ,{
	extend: 'Ext.panel.Panel',
	alias : 'widget.informationsettings',
	title : '',
	border: false,
	layout: 'fit',
	items: [{
        xtype: 'grouptabpanel',
        id: 'is-grouptabpanel',
    	border: 0,
        activeGroup: 0,
        items: [{
            mainItem: 0,
            expanded: true,
            items: [{
            	title : 'R scripts',
                tabTip: 'R scripts'
            }, {
				title : 'Script',
				xtype : 'scriptsgrid',
				iconCls : "icon-script-r"
			}]
        },{
            expanded: true,
            items: [{
            	title : 'Information Settings',
                tabTip: 'Information Settings'
            }, {
				title : 'Server',
				xtype : 'serversgrid',
				iconCls : "icon-servers"
			}, {
				title : 'Users',
				xtype : 'usersgrid',
				iconCls : "icon-client"
			}, {
				title : 'System Configuration',
				xtype : 'configsgrid',
				iconCls : "icon-config",
				hidden: _roles.indexOf("ROLE_ADMIN") != -1 ? false : true
			}]
        }]
    }],
	initComponent: function() {
		this.callParent(arguments);
	}
});
