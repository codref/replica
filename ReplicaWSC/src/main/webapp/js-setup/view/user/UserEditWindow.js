/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.view.user.UserEditWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.usereditwindow',
	width : 700,
	height : 480,
	minWidth : 300,
	minHeight : 200,
	layout : 'fit',
	plain : true,
	autoShow: false,
	requires : [ 'Ext.ux.form.ItemSelector' ],
	title: 'Edit User',
	initComponent : function() {

		var form = Ext.create('Ext.form.Panel', {
			bodyPadding : 10,
			border : false,
			items : [ {
				xtype : 'hidden',
				name : 'id'
			},{
				xtype : 'hidden',
				name : 'password'
			}, {
				xtype : 'textfield',
				name : 'username',
				anchor : '100%',
				fieldLabel : 'Username',
				allowBlank : false
			}, {
				xtype : 'textfield',
				name : 'plain_password',
				anchor : '100%',
				fieldLabel : 'Password',
				allowBlank : false
			}, {
				xtype : 'checkbox',
				name : 'enabled',
				fieldLabel : 'Enabled',
				uncheckedValue : 'off',
				allowBlank : false
			}, {
				xtype : 'itemselector',
				name : 'roles',
				anchor : '100%',
				height: 200,
				fieldLabel : 'Roles',
				buttons:['add', 'remove'],
				store : new Ext.data.ArrayStore({
			        fields: ['role'],
			        data : [['ROLE_USER'],['ROLE_ADMIN'],['ROLE_SETUP']]
			    }),
				displayField : 'role',
				valueField : 'role',
				allowBlank : false,
				msgTarget : 'side',
				fromTitle : 'Available',
				toTitle : 'Selected',
				ddReorder: false
			} ],
			buttons : [ {
				text : 'Save',
				action : 'save'
			} ]
		});

		Ext.apply(this, {
			items : form,
	        show: function(callback) {            
	        	var me = this;
	            Ext.Window.prototype.show.apply(this, arguments);
	            form.down('button[action=save]').on('click', function() {
	            	callback(me);	
	            });
	            
	        }     
		});

		this.callParent(arguments);
	}
});
