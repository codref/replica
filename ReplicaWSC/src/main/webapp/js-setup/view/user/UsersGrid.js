/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.view.user.UsersGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.usersgrid',
	title: 'Users',
	iconCls: 'icon-client',
	initComponent : function() {
		Ext.apply(this, {
			xtype : 'grid',
			store : 'Users',
			header: false,
			dockedItems : [{
				dock: 'bottom',
				xtype : 'toolbar',
				items : [{
					text : 'Add',
					iconCls : 'icon-add',
					action : 'add',
					scope : this
				}, {
					text : 'Edit',
					iconCls : 'icon-edit',
					action : 'edit',
					scope : this
				}, '-', {
					text : 'Delete',
					iconCls : 'icon-delete',
					action : 'delete'
				},'->', Ext.create('Ext.PagingToolbar', {
					store : 'Users',
					border: false,
					padding: '0 0 0 0',
					displayInfo : true,
					displayMsg : 'User(s) {0} - {1} di {2}',
					emptyMsg : "No users"
				})]
			}],
			columns : [{
				header : 'Username',
				dataIndex : 'username',
				flex : 0.8,
				field : {
					type : 'textfield',
					allowBlank : false
				}
			}, {
				header : 'Enabled',
				dataIndex : 'enabled',
				flex : 1.2,
				field : {
					type : 'textfield',
					allowBlank : true
				}
			}],
			tbar : [
			        {
			        	xtype: 'label',
			        	text: 'Users',
			        	cls: 'grid-title',
			        	margins: '0 0 0 10'
			        },
					'->',
					Ext.create('spc.ux.GridSearching', {
						grid : this,
						border: false,
						menuPosition : 'right',
						menuIconCls : 'icon-config',
						inputWidth : 200,
						padding: '0 0 0 0',
						showSelectAll : false,
						menuText : null,
						menuTip : 'Search configuration',
						inputTip : 'Write here to start the research',
						disableIndexes : [],
						items : []
					}) ]
		});
		
		this.callParent(arguments);
	}	
});
