/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.view.config.ConfigEditWindow', {
	extend : 'Ext.window.Window',
	alias: 'widget.configeditwindow',
	width:      500,
    height:     400,
    minWidth:   300,
    minHeight:  200,
    layout:     'fit',
    plain:      true,
    initComponent : function() {
    	
    	var form = Ext.create('Ext.form.Panel', {
            bodyPadding:    10,
            border: false,
            items: [{
                xtype:      'textfield',
                name:       'label',
                anchor:     '100%',
                fieldLabel: 'Label',
                allowBlank: false
            }, {
                xtype:      'codemirror',
                pathModes:  '/CodeMirror-2.24/mode',
                pathExtensions: '/CodeMirror-2.24/lib/util',
                name:       'value',
                fieldLabel: 'Value',
                anchor:     '100% -20',
                labelAlign: 'top',
                mode:       'text/javascript',
                listeners: {
                    modechanged: function(editor, newMode, oldMode){
                        switch(newMode){
                            case 'text/x-rsrc':
                                editor.setValue(Ext.get('codeR').getValue());
                            break;
                            
                            case 'application/json':
                                editor.setValue(Ext.get('codeJson').getValue());
                            break;
                            
                            case 'text/javascript':
                                editor.setValue(Ext.get('codeJs').getValue());
                            break;
                            
                            case 'text/html':
                                editor.setValue(Ext.get('codeHtml').getValue());
                            break;
                            
                            case 'text/css':
                                editor.setValue(Ext.get('codeCss').getValue());
                            break;
                            
                            case 'text/plain':
                            break;
                        }
                    }
                }
            }
            
            ],
            buttons: [{
                text: 'Save',
                action: 'save'            
            }]
		});    			
    	
    	Ext.apply(this, {
    		items: form
    	});
    	
    	this.callParent(arguments);
    }
});
