/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.view.server.ServerStatusWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.serverstatuswindow',
	width : 700,
	height : 480,
	minWidth : 300,
	minHeight : 200,
	layout : 'fit',
	plain : true,
	autoShow : false,
	title : 'Node Status',
	address : '',
	initComponent : function() {
		var me = this;

		Ext.apply(this, {
			items : [ {
				xtype : 'grid',
				store : 'ProcessesStatus',
				columns : [ {
					header : 'pid',
					dataIndex : 'pid',
					flex : 1,
				}, {
					header : '%cpu',
					dataIndex : 'pcpu',
					flex : 1,
				}, {
					header : '%mem',
					dataIndex : 'pmem',
					flex : 1,
				}, {
					header : 'start',
					dataIndex : 'start',
					flex : 1,
				}, {
					header : 'time',
					dataIndex : 'time',
					flex : 1,
				}, {
					header : 'args',
					dataIndex : 'args',
					flex : 2,
				}, {
			        xtype : 'actioncolumn',
			        header : '',
			        width : 30,
			        align : 'center',
			        sortable: false,
	                menuDisabled: true,
			        items : [
			            {
			                iconCls:'icon-reset',
			                tooltip : 'Kill process',
			                handler: function(grid, rowIndex, colIndex, item, e) {
			                	var rec = grid.getStore().getAt(rowIndex);
	                            this.fireEvent('itemclick', this, grid, rec, rowIndex, colIndex, item, e);
	                        },
			            }
			        ]
			    } ]
			} ],
	        show: function(address) {
	        	me.address = address;	        	
	        	Ext.Window.prototype.show.apply(this, arguments);
	        } 
		});

		this.callParent(arguments);
	}
});
