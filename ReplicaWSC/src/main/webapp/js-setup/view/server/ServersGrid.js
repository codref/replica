/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.view.server.ServersGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.serversgrid',
	title: 'Servers',
	requires : [ 
	             'Ext.grid.plugin.CellEditing'
	],
	iconCls: 'icon-client',
	initComponent : function() {
		var me = this,
			editing = Ext.create('Ext.grid.plugin.CellEditing');
		Ext.apply(this, {
			xtype : 'grid',
			store : 'Servers',
		    editing: editing,
		    plugins : [ editing ],
			header: false,
			dockedItems : [{
				dock: 'bottom',
				xtype : 'toolbar',
				items : [{
					text : 'Add',
					iconCls : 'icon-add',
					action : 'add',
					scope : this
				}, {
					text : 'Delete',
					iconCls : 'icon-delete',
					action : 'delete'
				},'->', Ext.create('Ext.PagingToolbar', {
					store : 'Servers',
					border: false,
					padding: '0 0 0 0',
					displayInfo : true,
					displayMsg : 'Server(s) {0} - {1} di {2}',
					emptyMsg : "No servers"
				})]
			}],
			columns : [{
				header : 'Address',
				dataIndex : 'address',
				flex : 1,
				field : {
					type : 'textfield',
					allowBlank : false
				}
			}, {
				header : 'Port',
				dataIndex : 'port',
				flex : 0.5,
				field : {
					type : 'textfield',
					allowBlank : false
				}
			}, {
				header : 'Weight',
				dataIndex : 'weight',
				flex : 0.1,
				field : {
					type : 'textfield',
					allowBlank : true
				}
			}, {
		        xtype : 'actioncolumn',
		        header : '',
		        width : 50,
		        align : 'center',
		        sortable: false,
                menuDisabled: true,
		        items : [
		            {
		                iconCls:'icon-mm',
		                tooltip : 'View node status',
		                id: 'action-status',
		                handler: function(grid, rowIndex, colIndex, item, e) {
		                	var rec = grid.getStore().getAt(rowIndex);
                            this.fireEvent('statusclick', this, grid, rec, rowIndex, colIndex, item, e);
                        },
		            }, {
		                iconCls:'icon-arrow-circle',
		                tooltip : 'Restart',
		                action: 'restart',
		                handler: function(grid, rowIndex, colIndex, item, e) {
		                	var rec = grid.getStore().getAt(rowIndex);
                            this.fireEvent('restartclick', this, grid, rec, rowIndex, colIndex, item, e);
                        },
		            }
		        ]
		    }],
			tbar : [
			        {
			        	xtype: 'label',
			        	text: 'Servers',
			        	cls: 'grid-title',
			        	margins: '0 0 0 10'
			        },
					'->',
					Ext.create('spc.ux.GridSearching', {
						grid : this,
						border: false,
						menuPosition : 'right',
						menuIconCls : 'icon-config',
						inputWidth : 200,
						padding: '0 0 0 0',
						showSelectAll : false,
						menuText : null,
						menuTip : 'Search server',
						inputTip : 'Write here to start the research',
						disableIndexes : [],
						items : []
					}) ]
		});
		
		this.callParent(arguments);
	}	
});
