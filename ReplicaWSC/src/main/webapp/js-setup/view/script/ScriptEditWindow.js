/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.view.script.ScriptEditWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.scripteditwindow',
	requires : [ 
	             'Ext.grid.plugin.CellEditing', 
	             'Ext.form.field.Text',
	             'Ext.toolbar.TextItem' 
	],
	width : 500,
	height : 500,
	minWidth : 300,
	minHeight : 200,
	maximizable: true,
	layout : 'fit',
	plain : true,
	title: 'Edit R script',
	autoShow: false,
	initComponent : function() {

		var scriptContentForm = Ext.create('Ext.form.Panel', {
			id: 'script-content-form',
			bodyPadding : 10,
			border : false,
			title: 'Name and content',
			items : [ {
				xtype : 'textfield',
				name : 'id',
				width: 200,
				fieldLabel : 'Script ID',
				readOnly : true
			}, {
				xtype : 'textfield',
				name : 'name',
				anchor : '100%',
				fieldLabel : 'Name',
				allowBlank: false
			}, {
				xtype : 'textfield',
				name : 'description',
				anchor : '100%',
				fieldLabel : 'Description'
			}, {
				xtype : 'codemirror',
				pathModes : '/CodeMirror-2.24/mode',
				pathExtensions : '/CodeMirror-2.24/lib/util',
				name : 'content',
				fieldLabel : 'Content',
				anchor : '100% -80',
				labelAlign : 'top',
				mode : 'text/x-rsrc',
				listeners : {
					modechanged : function(editor, newMode, oldMode) {
						switch (newMode) {
						case 'text/x-rsrc':
							editor.setValue(Ext.get('codePhp').getValue());
							break;

						case 'application/json':
							editor.setValue(Ext.get('codeJson').getValue());
							break;

						case 'text/html':
							editor.setValue(Ext.get('codeHtml').getValue());
							break;

						case 'text/css':
							editor.setValue(Ext.get('codeCss').getValue());
							break;

						case 'text/plain':
							break;
						}
					}
				}
			}

			],
			buttons : [ {
				text : 'Save',
				action : 'save'
			} ]
		});
		var editing = Ext.create('Ext.grid.plugin.CellEditing');
		var outputGrid =  Ext.create('Ext.grid.Panel', {    
			title: 'Output',
			id: 'output-grid-panel',
		    store: 'Outputs',
		    editing: editing,
		    plugins : [ editing ],
		    dockedItems : [{
				dock: 'bottom',
				xtype : 'toolbar',
				border: false,
				items : [{
					text : 'Add',
					iconCls : 'icon-add',
					action : 'add',
					scope : this
				}, '-', {
					text : 'Delete',
					iconCls : 'icon-delete',
					action : 'delete'
				},'->', Ext.create('Ext.PagingToolbar', {
					store : 'Outputs',
					border: false,
					padding: '0 0 0 0',
					displayInfo : true,
					displayMsg : 'Output(s) {0} - {1} di {2}',
					emptyMsg : "No output defined"
				})]
			}],
		    columns: [{
				header : 'Variable',
				dataIndex : 'variable',
				flex : 1,
				field : {
					type : 'textfield',
					allowBlank : false
				}
			}, {
				header : 'Code',
				dataIndex : 'code',
				flex : 2,
				field : {
					type : 'textfield',
					allowBlank : true
				}
			}],
		    height: 200,
		    width: 400,});
		

		// TODO define and insert Search field on grid toolbar 
		
		Ext.apply(this, {
			items : [{
				xtype: 'panel',
				border: false,
				layout: {
			        type: 'accordion',
			        titleCollapse: true,
			        animate: true,
			        activeOnTop: true
			    },
			    items: [scriptContentForm, outputGrid]
			}],
	        show: function(saveCallback, closeCallback) {            
	        	var me = this;
	        	outputGrid.getStore().load();
	            Ext.Window.prototype.show.apply(this, arguments);
	            scriptContentForm.down('button[action=save]').on('click', function() {
	            	saveCallback(me);
		            me.on('close', function(){
		            		closeCallback(me);
		            });
	            });
	        } 
		});

		this.callParent(arguments);
	}
});
