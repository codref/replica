/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.view.script.ScriptsGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.scriptsgrid',
	requires : [ 
	             'Ext.form.field.Text',
	             'Ext.toolbar.TextItem' 
	           ],
	title: 'Configuration',
	iconCls: 'icon-config',
	initComponent : function() {
		Ext.apply(this, {
			xtype : 'grid',
			store : 'Scripts',
			header: false,
			dockedItems : [{
				dock: 'bottom',
				xtype : 'toolbar',
				items : [{
					text : 'Edit',
					iconCls : 'icon-edit',
					action : 'externaledit',
					scope : this
				}, {
					text : 'Add',
					iconCls : 'icon-add',
					action : 'add',
					scope : this
				}, '-', {
					text : 'Delete',
					iconCls : 'icon-delete',
					action : 'delete'
				}, '->', Ext.create('Ext.PagingToolbar', {
					store : 'Scripts',
					border: false,
					padding: '0 0 0 0',
					displayInfo : true,
					displayMsg : 'Script(s) {0} - {1} di {2}',
					emptyMsg : "Table is empty"
				})]
			}],
			columns : [{
				header : 'Id',
				dataIndex : 'id',
				flex : 1,
				field : {
					type : 'textfield'
				}
			},{
				header : 'Name',
				dataIndex : 'name',
				flex : 3,
				field : {
					type : 'textfield'
				}
			},{
				header : 'Description',
				dataIndex : 'description',
				flex : 8,
				field : {
					type : 'textfield'
				}
			}],
			tbar : [{
			        	xtype: 'label',
			        	text: 'Deployed R scripts',
			        	cls: 'grid-title',
			        	margins: '0 0 0 10'
			        },
					'->',
					Ext.create('spc.ux.GridSearching', {
						grid : this,
						border: false,
						menuPosition : 'right',
						menuIconCls : 'icon-config',
						inputWidth : 200,
						padding: '0 0 0 0',
						showSelectAll : false,
						menuText : null,
						menuTip : 'Search',
						inputTip : 'Write here to start the research',
						disableIndexes : [],
						items : []
					}) ]
		});
		
		this.callParent(arguments);
	}	
});
