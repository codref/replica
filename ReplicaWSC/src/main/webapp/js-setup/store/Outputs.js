/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.store.Outputs', {
	extend : 'Ext.data.Store',
	model : 'setup.model.Output',
	autoLoad : false,
	autoSync : true,
	pageSize : 50,
	remoteSort : true,
	proxy : {
		type : 'ajax',
		api : {
			read : _instance_path_ + '/setup/script/output/view.action',
			create : _instance_path_ + '/setup/script/output/create.action',
			update : _instance_path_ + '/setup/script/output/update.action',
			destroy : _instance_path_ + '/setup/script/output/delete.action'
		},
		reader : {
			type : 'json',
			totalProperty : 'total',
			successProperty : 'success',
			idProperty : 'id',
			root : 'data',
			messageProperty : 'message',
		},
		writer : {
			type : 'json',
			encode : true,
			root : 'data'
		},
		extraParams : {
			scriptId : ''
		}

	},
	listeners : {
		write : function(proxy, operation) {
			if (operation.action == 'create')
				Ext.example.msg('Done', 'Output inserted');
			else if (operation.action == 'update')
				Ext.example.msg('Done', 'Output updated');
			else if (operation.action == 'destroy')
				Ext.example.msg('Done', 'Outputs removed');
		}
	}
});