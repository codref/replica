/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.Loader.setConfig({
	enabled : true
});

Ext.Loader.setPath('Ext.ux', _resources_url + '/ext-4.2.1.883/examples/ux');
Ext.Loader.setPath('spc.ux', _instance_path_ + '/js-components/spc/ux');

Ext.require([ 
             'Ext.window.MessageBox', 
             'Ext.tip.*', 
             'Ext.ux.statusbar.StatusBar',
             'spc.ux.InstanceLogWindow',
             'spc.ux.TimeoutChecker',
             'spc.ux.GridSearching',
             'spc.ux.Download',
             'Ext.ux.GroupTabPanel'
             ]);

Ext.application({
	name : 'setup',
	appFolder : _instance_path_ + '/js-setup',

	controllers : [  
	    'TabsView',
	    'MainMenu',
	    'User', 
	    'Config',
	    'Script',
	    'Server',
	    'InformationSettingsTabsView',
	    'Terminal'
	],
	
	instanceLogWindow : Ext.create('spc.ux.InstanceLogWindow'),
	
	timeoutChecker : Ext.create('spc.ux.TimeoutChecker'),

	launch : function() {
		setup.app = this;
		var me = this;
		
		// bind session timeout checker
    	Ext.Ajax.on('beforerequest', me.timeoutChecker.refresh);
    	
    	// init application
		Ext.create('Ext.container.Viewport', {
			layout: 'border',
			items: [{
				layout: 'column',
				region: 'north',
				border: false,
				id: 'panel-header',
				items: [ {
		        	xtype: 'headerMenu',
		        	border: false,
		        	margin: '0 0 0 0',
		            columnWidth: 1
		        }]
			},{
				xtype: 'panel',
				header: false,
				title: 'Replica Web Service Setup',
				border: 0,
				region: 'center',
				layout : 'border',
				items : [ {
					id : 'downloader',
					xtype : 'download'
				}, {
		        	xtype: 'box',
		        	region: 'north',
		        	height: 3,
		        	margin: '0 0 0 0'
		        }, {
					xtype : 'container',
					region : 'center',
					layout : 'border',
					border : false,
					items : [ {
						xtype : 'tabsview',
						region : 'center',
						border: false
					} ]
				}],
				bbar: Ext.create('Ext.ux.StatusBar', {
					cls: 'blue-bbar',

			        text: '<span style="font-family: Roboto, sans-serif; color:#000000; font-size: 1.4em; font-weight: 200;">Replica Web Service</span> <span style="font-family: Roboto, sans-serif; color:#000000;font-weight: 200;">version 1.0</span>',
					iconCls: 'ready-icon',

					items: [ {
						xtype: 'button',
						text: 'Logs',
						hidden: _roles.indexOf("ROLE_ADMIN") != -1 ? false : true,
						handler: function (){
							me.instanceLogWindow.show();
						}
					}, '-', {
						xtype: 'button',
						text: 'Logout',
						baseCls: 'logout-btn',
						handler: function (){
							document.location.href = _logout_url;
						}
					},  '<span style="font-family: Roboto, sans-serif; color:#000000;">' + _username + '</span>']
				})		
			}]			
		});
	}
});

