/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.controller.Config', {
    extend: 'Ext.app.Controller',
    stores: ['Configs'],
    models: ['Config'],
    views: [
        'config.ConfigsGrid',
        'config.ConfigEditWindow'
    ],
    
	refs: [{
		ref: 'editWindow',
		selector : 'configeditwindow'
	},{
		ref: 'grid',
		selector: 'configsgrid'
	}],    

    init: function() {
        this.control({
        	'configsgrid' : {
        		beforerender: this.loadData,
        		itemdblclick: this.onEditClick
        	},        	
            'configsgrid button[action=externaledit]' : {
                click: this.onEditClick
            },        	
            'configsgrid button[action=add]' : {
                click: this.onAddClick
            },
            'configsgrid button[action=delete]' : {
                click: this.configDelete
            },
            'configsgrid button[action=edit]' : {
                click: this.clientEdit
            }, 
            'configeditwindow form button[action=save]' :{
            	click: this.save
            }
        });
    },
    
    onEditClick: function() {
    	var w = Ext.widget('configeditwindow');
    	w.down('form').getForm().loadRecord(this.getGrid().getSelectionModel().getSelection()[0]);
    	w.show();
    },
    
    onAddClick: function(grd) {  
      var edit;
      var grid = Ext.ComponentQuery.query('configsgrid')[0];
      var s = grid.store;
      var r = new (s.model)({
            name: ''});
      edit = Ext.ComponentQuery.query('configsgrid')[0].editing;
      s.insert(0, r);
      edit.startEditByPosition({row: 0, column: 0});
    },
    
    configDelete: function(sender, event) {
        var store = this.getConfigsStore();
        Ext.MessageBox.confirm('','Are you sure?', function(button){
            if (button == 'yes') {
                var grid = Ext.ComponentQuery.query('configsgrid')[0];
                var sm = grid.getSelectionModel();
                store.remove(sm.getSelection());                
                var selection = Ext.ComponentQuery.query('configsgrid')[0].getView().getSelectionModel().deselectAll();
            }
        });
    },

	clientEdit : function(grd, selectedRecord) {
		edit = Ext.ComponentQuery.query('configsgrid')[0].editing;
		var i = edit.grid.selModel.lastSelected.index;
		edit.startEditByPosition({
			row : i,
			column : 0
		});
	},
    
    loadData: function(grd){
    	var store = this.getConfigsStore();
    	store.load();
    },
    
    save: function() {
    	var form = this.getEditWindow().down('form');
        if(form.getForm().isValid()){
        	var record = form.getRecord(),
        	    values = form.getValues();
        	record.set(values);
        	this.getEditWindow().close();
        }
    }
    
	
});
