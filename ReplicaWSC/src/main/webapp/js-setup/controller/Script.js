/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.controller.Script', {
    extend: 'Ext.app.Controller',
    stores: ['Scripts', 'Outputs'],
    models: ['Script', 'Output'],
    views: [
        'script.ScriptsGrid',
        'script.ScriptEditWindow'
    ],
    
	refs: [{
		ref: 'editWindow',
		xtype : 'scripteditwindow',
		forceCreate: true
	},{
		ref: 'grid',
		selector: 'scriptsgrid'
	},{
		ref: 'outputGrid',
		selector: 'scripteditwindow grid'
	}],    

    init: function() {
        this.control({
        	'scriptsgrid' : {
        		itemdblclick: this.editDblClick
        	},        	     	
            'scriptsgrid button[action=add]' : {
                click: this.addClick
            },
            'scriptsgrid button[action=delete]' : {
                click: this.scriptDelete
            },
            'scripteditwindow button[action=add]' : {
            	click: this.outputAddClick
            },
            'scripteditwindow button[action=delete]' : {
            	click: this.outputDeleteClick
            }
        });
    },
    
    editDblClick: function() {
    	var me = this,
    		w = me.getEditWindow(),
    		scriptGrid = me.getGrid(),
    		selectedRecord = scriptGrid.getSelectionModel().getSelection()[0];
    	
    	me.getOutputsStore().getProxy().setExtraParam('scriptId', selectedRecord.data.id);
        Ext.Ajax.request({
            url: _instance_path_ + '/setup/script/viewSingleScript.action', 
            method: 'POST',
            params  : {
            	scriptId : selectedRecord.data.id
            },
            success: function(response){
            	// TODO manage JSON success
            	var ajaxResponse = Ext.decode(response.responseText),
            		r = new setup.model.Script(ajaxResponse.data[0]);
            		form = w.down('#script-content-form').getForm();
            	form.loadRecord(r);
            	w.show(function(w) {
            		var form = w.down('form').getForm();
                    Ext.Ajax.request({
                        url: _instance_path_ + '/setup/script/update.action', 
                        method: 'POST',
                        params  : form.getValues(),
                        success: function(response){
                        	// TODO manage JSON success
                        }, 
                        failure: function(data){
                        	// TODO show error
                        }
                    });
                    
            	}, function(w) {
            		// TODO ExtJS bug here!
            		if (w != undefined)
            			me.getScriptsStore().reload();
            	});
            }, 
            failure: function(data){
            	// TODO show error
            }
        });
    },
    
    addClick: function(grd) {  
    	var me = this,
		w = me.getEditWindow(),
		outputGridPanel = w.down('#output-grid-panel');
    	outputGridPanel.setDisabled(true);
    	w.show(function(w) {
    		var form = w.down('form').getForm();
            Ext.Ajax.request({
                url: _instance_path_ + '/setup/script/create.action', 
                method: 'POST',
                params  : form.getValues(),
                success: function(response){
                	// TODO manage JSON success
                	var ajaxResponse = Ext.decode(response.responseText),
                		outputsStore = me.getOutputsStore();
                	outputsStore.getProxy().setExtraParam('scriptId', ajaxResponse.data[0].id);
                	outputsStore.reload();	
                	w.down('form textfield[name=id]').setValue(ajaxResponse.data[0].id);
                	outputGridPanel.setDisabled(false);
                }, 
                failure: function(data){
                	// TODO show error
                }
            });
    	}, function(w) {
    		me.getScriptsStore().reload();
    	});
    },
    
    scriptDelete: function(sender, event) {
    	var me = this;
        Ext.MessageBox.confirm('','Continue with the operation?', function(button){
            if (button == 'yes') {
                var grid = me.getGrid(),
            		store = me.getScriptsStore(),
            		sm = grid.getSelectionModel();
                store.remove(sm.getSelection());                
                sm.deselectAll();
            }
        });
    },

    
    outputAddClick: function(sender) {
        var me = this,
        	outputStore = me.getOutputsStore(),
        	scriptId = sender.up('window').down('form textfield[name=id]').getValue();
        	r = new (outputStore.model)({
        		script : scriptId
        	}),
        	edit = me.getOutputGrid().editing;
        outputStore.insert(0, r);
        edit.startEditByPosition({row: 0, column: 0});
    },
    
    outputDeleteClick: function() {
    	var me = this;    		
        Ext.MessageBox.confirm('','Continue with the operation?', function(button){
            if (button == 'yes') {
                var grid = me.getOutputGrid(),
                	outputStore = me.getOutputsStore(),
                	sm = grid.getSelectionModel();
                outputStore.remove(sm.getSelection()); 
                sm.deselectAll();
            }
        });
    }
});
