/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.controller.Server', {
    extend: 'Ext.app.Controller',
    stores: ['Servers', 'ProcessesStatus'],
    models: ['Server'],
    views: [
        'server.ServersGrid',
        'server.ServerStatusWindow'
    ],
    
	refs: [{
		ref: 'serversGrid',
		selector: 'serversgrid'
	},{
		ref: 'serverStatusWindow',
		xtype: 'serverstatuswindow',
		forceCreate: true
	}],    

    init: function() {
        this.control({
        	'serversgrid button[action=add]' : {
        		click: this.serverAddClick
        	},
        	'serversgrid button[action=delete]' : {
        		click: this.serverDeleteClick
        	},
        	'serversgrid actioncolumn' : {
        		statusclick: this.viewNodeStatus,
        		restartclick: this.restartRserveProcess
        	},
        	'serverstatuswindow actioncolumn' : {
        		itemclick: this.killRserveProcess
        	}
        });
    },
            
    restartRserveProcess : function (column, grid, record, rowIndex, colIndex, item, e) {
    	 Ext.Ajax.request({
	            url: _instance_path_ + '/setup/server/processRestart.action', 
	            method: 'GET',
	            params  : {
	            	'address' : record.get('address'),
	            	'command' : record.get('command')
	            },
	            success: function(response){
	            	alert(response.statusText);
	            }, 
	            failure: function(data){
	            	alert(response.statusText);
	            }
	        });
    },
    
    killRserveProcess: function (column, grid, record, rowIndex, colIndex, item, e) {
    	var me = this,
    		pid = record.get('pid'),
    		processesStore = me.getProcessesStatusStore(),
    		address = grid.up('serverstatuswindow').address;
    		
        Ext.Ajax.request({
            url: _instance_path_ + '/setup/server/processKill.action', 
            method: 'GET',
            params  : {
            	'address' : address,
            	'pid' : pid
            },
            success: function(response){
            	alert(response.statusText);
            	processesStore.reload();
            }, 
            failure: function(data){
            	alert(response.statusText);
            }
        });
    },
    
    viewNodeStatus:  function(column, grid, record, rowIndex, colIndex, item, e) {
    	var me = this,
    		address = record.get('address'),
    		processesStore = me.getProcessesStatusStore();
    	processesStore.getProxy().setExtraParam('address', address);
    	processesStore.reload();
    	me.getServerStatusWindow().show(address);
    },
    
    serverAddClick: function(sender) {
        var me = this,
        	serversStore = me.getServersStore(),
        	r = new (serversStore.model)(),
        	edit = me.getServersGrid().editing;
        serversStore.insert(0, r);
        edit.startEditByPosition({row: 0, column: 0});
    },
    
    serverDeleteClick: function() {
    	var me = this;    		
        Ext.MessageBox.confirm('','Continue with the operation?', function(button){
            if (button == 'yes') {
                var grid = me.getServersGrid(),
                	serversStore = me.getServersStore(),
                	sm = grid.getSelectionModel();
                serversStore.remove(sm.getSelection()); 
                sm.deselectAll();
            }
        });
    }
});
