/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.controller.TabsView', {
    extend: 'Ext.app.Controller',
    views: [ 'TabsView' ],    
    init: function() { 
    	this.control({
    		'#setup-tabview' : {
    			afterrender: this.changeHeaderBg
    		}
    	});
    },
    
    changeHeaderBg: function(tabPanel, newCard, oldCard, eOpts) {
    		Ext.get('panel-header-body').setStyle('backgroundColor','#3892d3');
    		Ext.get('home-header-menu-body').setStyle('backgroundColor','#f2f2f2');
    }
});
