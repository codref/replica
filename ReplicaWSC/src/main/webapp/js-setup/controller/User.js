/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('setup.controller.User', {
    extend: 'Ext.app.Controller',
    stores: ['Users'],
    models: ['User'],
    views: [
        'user.UsersGrid',
        'user.UserEditWindow'
    ],
    
    refs: [{
		ref: 'usersGrid',
		selector : 'usersgrid'
	}, {
		ref: 'editWindow',
		xtype: 'usereditwindow',
		forceCreate: true
	}],
	
    init: function() {
        this.control({
        	'usersgrid' : {
        		beforerender: this.loadData,
        		itemdblclick: this.userEdit        		
        	},
            'usersgrid button[action=add]' : {
                click: this.onAddClick,
            },
            'usersgrid button[action=delete]' : {
                click: this.userDelete
            },
            'usersgrid button[action=edit]' : {
                click: this.userEdit
            }
        });
    },
    
    onAddClick: function(grd) {  
    	var me = this;
    	me.getEditWindow().show(function(w) {
    		var form = w.down('form').getForm();
            Ext.Ajax.request({
                url: _instance_path_ + '/setup/user/create.action', 
                method: 'POST',
                params  : form.getValues(),
                success: function(response){
                	// TODO manage JSON success
                	w.close();
                }, 
                failure: function(data){
                	// TODO show error
                }
            });
            me.getUsersStore().reload();
    	});
    },
    
    userDelete: function(sender, event) {
        var me = this, store = this.getUsersStore();
        Ext.MessageBox.confirm('','Are you sure?', function(button){
            if (button == 'yes') {
                var grid = me.getUsersGrid();
                var sm = grid.getSelectionModel();
                store.remove(sm.getSelection());                
                grid.getView().getSelectionModel().deselectAll();
            }
        });
    },

	userEdit : function(grd, selectedRecord) {
    	var me = this, 
    		w = me.getEditWindow(),
    		record = this.getUsersGrid().getSelectionModel().getSelection()[0],
    		form = w.down('form').getForm(); 	
    	form.loadRecord(record);
        Ext.Ajax.request({
            url: _instance_path_ + '/setup/user/viewRoles.action', 
            method: 'POST',
            params  : {'userId': record.data.id},
            success: function(response){
            	// TODO manage JSON success
                var ajaxResponse = Ext.decode(response.responseText);
                form.setValues({
                	roles : ajaxResponse.data
                });
            }, 
            failure: function(data){
            	// TODO show error
            }
        });
    	w.show(function(w) {
    		var form = w.down('form').getForm();
            Ext.Ajax.request({
                url: _instance_path_ + '/setup/user/update.action', 
                method: 'POST',
                params  : form.getValues(),
                success: function(response){
                	// TODO manage JSON success
                	w.close();
                	me.getUsersStore().reload();
                }, 
                failure: function(data){
                	// TODO show error
                }
            });
    	});   
	},
    
    loadData: function(grd){
    	var store = this.getUsersStore();
    	store.load();
    }
    
});
