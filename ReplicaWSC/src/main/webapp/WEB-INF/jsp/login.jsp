<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html;charset=UTF-8" language="java"%>
<%@page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SPC Setup</title>

<!-- Custom Fonts Setup via Font-face CSS3 -->
<link href="fonts/fonts.css" rel="stylesheet">

<style>
body {
	height: 100%;
}

.login-body {
	padding-bottom: 80px;
	height: 100%;
}

.login-footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	height: 80px;
	background-color: #F9FBFB;
	border-top: 1px solid #E8EBEE;
	color: #7B8188;
	padding: 20px 0 40px;
	font-family: 'Roboto', sans-serif;
	font-weight: 100;
}

.errorblock {
	color: #ff0000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}

.login-header {
	background-color: #3892d3;
	height: 68px;
}

.login-header h1 {
	font-weight: 100;
	font-family: 'Roboto', sans-serif;
	font-size: 3.2em;
	color: #FFF;
	margin: 0;
	padding-top: 3px;
}

.login-container {
	text-align: center;
	margin: auto;
	margin-top: 100px;
}

a.login-link:link, a.login-link:visited, a.login-link:hover, a.login-link:active {
	text-decoration:none;
	color: #7B8188;
}

.inner {
	margin: 0 auto;
	overflow: hidden;
	width: 960px;
}

.onLeft {
	float: left;
}

.onRight {
	float: right;
}
</style>


<!-- ExtJS css -->
<link rel="stylesheet" type="text/css"
	href="${resources_url}/ext-4.2.1.883/resources/css/ext-all-neptune.css" />

<!-- ExtJS js -->
<script src="${resources_url}/ext-4.2.1.883/ext-all.js"></script>

<script type="text/javascript">
	//<![CDATA[

	Ext.onReady(function() {
		var loginForm = new Ext.FormPanel({
			standardSubmit : true,
			border : false,
			padding: '0 10 10 10',
			fieldDefaults: {
				margin: '16 0 0 0',
				labelAlign: 'top',
				labelStyle: 'font-weight: bold; color: #474747',
				labelSeparator: ''
			},
	      layout: {
	            type: 'vbox',
	            align: 'stretch'  // Child items are stretched to full width
         },
			defaultType : 'textfield',
			bodyPadding : 10,
			items : [ {
				fieldLabel : 'Username',
				name : 'j_username'
			}, {
				inputType : 'password',
				fieldLabel : 'Password',
				name : 'j_password',
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == e.ENTER) {
							submitLogin();
						}
					}
				}
			}, {
				inputType : 'hidden',
				hidden : true,
				id : 'submitbutton',
				name : 'myhiddenbutton',
				value : 'hiddenvalue'
			} ]

		});

	var win = Ext.create('Ext.window.Window', {
			title : 'Authentication Required',
			width : 300,
			height : 250,
			layout : 'fit',
			plain : true,
			closable : false,
			resizable : false,
			border: false,
			items : loginForm,
			buttons : [ {
				text : 'Login',
				scope : this,
				handler : function() {
					submitLogin();
				}
			}, {
				id : 'lf.btn.reset',
				text : 'Reset',
				handler : function() {
					fnResetForm(loginForm);
				}
			} ]
		});

		function submitLogin() {
			var form = loginForm.getForm();
			form.url = 'j_spring_security_check';
			form.method = 'POST';
			if (form.isValid()) {
				form.submit();
			}
		}

		function fnResetForm(theForm) {
			theForm.getForm().reset();
		} //end fnResetForm		    

		win.show();
	});

	//
</script>

</head>

<body>
	<div class="login-header">
		<div class="inner">
			<h1>Replica Web Service</h1>
		</div>
	</div>

	<div class="login-body">
		<c:if test="${not empty error}">
			<div class="errorblock">
				Your login attempt was not successful, try again.<br /> Caused :
				${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
			</div>
		</c:if>
		<div id="login-container"></div>
	</div>

	<div class="login-footer">
	   <div class="inner">
   		<p class="onLeft"><span style="font-size: 1.4em">Replica Web Service</span> version 1.0</p>
   		<p class="onRight" style="padding-top:4px;"><a href="http://www.kpa-group.com" class="login-link">www.codref.com</a></p>
   	</div>
	</div>

</body>
</html>