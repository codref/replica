<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>SPC Setup</title>
	
	<!-- Custom Fonts Setup via Font-face CSS3 -->
	<link href="/${instance_name}/fonts/fonts.css" rel="stylesheet">
	
	<!-- ExtJS css -->
	<link rel="stylesheet" type="text/css" href="${resources_url}/ext-4.2.1.883/resources/css/${extjs_debug == 'true' ? 'ext-all-neptune-debug.css' : 'ext-all-neptune.css'}" />
	<link rel="stylesheet" type="text/css" href="${resources_url}/ext-4.2.1.883/examples/shared/example.css" />
	<link rel="stylesheet" type="text/css" href="${resources_url}/CodeMirror-2.24/lib/codemirror.css" />
	<link rel="stylesheet" type="text/css" href="/${instance_name}/css/portal.css" />	
    <link rel="stylesheet" type="text/css" href="${resources_url}/ext-4.2.1.883/examples/ux/css/GroupTabPanel.css" />
	
	
	<!-- ExtJS js -->
	<script src="${resources_url}/ext-4.2.1.883/${extjs_debug == 'true' ? 'ext-all-dev.js' : 'ext-all.js'}"></script>
	<script src="${resources_url}/ext-4.2.1.883/examples/shared/examples.js"></script>
	<script src="${resources_url}/CodeMirror-2.24/lib/codemirror.js"></script>
	<script src="/${instance_name}/js-components/CodeMirror/Ext.ux.form.field.CodeMirror.411.js"></script>

	<script type="text/javascript">
	//<![CDATA[
	    var _resources_url = '${resources_url}';
		var _instance_name_ = '${instance_name}'; 
		var _instance_path_ = '/' + _instance_name_;
		var _logout_url = '<c:url value="/j_spring_security_logout" />';
		var _username = '${username}';
		var _roles = '${roles}';
		var _current_application = 'setup';
	//]]>
	</script>	
	
	<!-- App js -->
	<script src="/${instance_name}/js-setup/app-setup.js"></script>
	
</head>
<body>
	<div id="content"></div>
</body>
</html>