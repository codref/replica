/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function($) {
	$.fn.cli = function(handler, prompt, effect) {
		var history = [], hPointer = null;


		var commands = ['history', 'replica.authenticate', 'replica.execscript', 'replica.wget'];

		if (!prompt)
			prompt = '&gt;&nbsp;';

		if (!effect)
			effect = type;

		function getHistory() {
			var result = '';
			for (var i = history.length - 1; i >= 0; i--) {
				result += history[i] + '<br />';
			}
			return result;
		};

		function prevHistory() {
			hPointer = (hPointer - 1) % history.length;
			return history[hPointer];
		}

		function nextHistory() {
			hPointer = (hPointer + 1) % history.length;
			return history[hPointer];
		}
		
		function getHelp() {
			return 'help - this command<br />\
			history - show last inserted commands<br />\
			replica.authenticate(host, username, password) - return a valid authentication token from a Replica Web Service server<br />\
			replica.execscript(host, scriptName, authtoken, input) - execute a script <br />\
			replica.wget(url) - open the resource located at the defined url on a new browser window';
		}
		
		function getAllMethods(object) {
			// http://stackoverflow.com/a/2946616
			return Object.getOwnPropertyNames(object).filter(function(property) {
				return typeof object[property] == 'function';
			});
		}

		function autocomplete(input, data) {
			// http://stackoverflow.com/a/1897480
			var candidates = [];
			for (var i = 0; i < data.length; i++) {
				if (data[i].indexOf($(input).html()) === 0 && data[i].length > $(input).html().length)
					candidates.push(data[i]);
			}

			if (candidates.length > 0) {
				if (candidates.length == 1)
					$(input).text(candidates[0]);
				else
					$(input).text(longestInCommon(candidates, $(input).html().length));
				return true;
			}
			return false;
		}

		function longestInCommon(candidates, index) {
			var i, ch, memo;
			do {
				memo = null;
				for ( i = 0; i < candidates.length; i++) {
					ch = candidates[i].charAt(index);
					if (!ch)
						break;
					if (!memo)
						memo = ch;
					else if (ch != memo)
						break;
				}
			} while (i == candidates.length && ++index);

			return candidates[0].slice(0, index);
		}

		function setEndOfContenteditable(contentEditableElement) {
			// http://stackoverflow.com/questions/1125292/how-to-move-cursor-to-end-of-contenteditable-entity/3866442#3866442
			var range, selection;
			if (document.createRange) //Firefox, Chrome, Opera, Safari, IE 9+
			{
				range = document.createRange();
				range.selectNodeContents(contentEditableElement);
				range.collapse(false);
				selection = window.getSelection();
				selection.removeAllRanges();
				selection.addRange(range);
			} else if (document.selection) { //IE 8 and lower 
				range = document.body.createTextRange();
				range.moveToElementText(contentEditableElement);
				range.collapse(false);
				range.select();
			}
		}

		return this.each(function() {
			var self = $(this);
			function newline() {
				self.append('<p class=input><span class=prompt>' + prompt + '</span><span  style=outline:none contenteditable></span></p>');
				try {
					$('[contenteditable]', self)[0].focus(); // focus only works on the element, not the jQuery object
				} catch(e) {
					/* do nothing. IE throws and error if the focus cannot be set */
				}
			}

			newline();

			self.on('keydown', '[contenteditable]', function(e) {
				var code = e.keyCode || e.which;


				if (code == 13) { 	// return
					var commandText = this.textContent || this.innerText;
					if (commandText == '')
						return false;

					history.unshift(commandText);
					hPointer = history.length - 1;

					// process help command
					if (commandText == 'help') {
						$(this).removeAttr('contenteditable');
						effect.call($('<p class=response>').appendTo(self), getHelp());
						
					// process CLI embedded commands
					} else if (commandText == 'history') {
						$(this).removeAttr('contenteditable');
						effect.call($('<p class=response>').appendTo(self), getHistory());

					// process generic command
					} else {
						$(this).removeAttr('contenteditable');
						// IE needs special handling which jQuery provides
						effect.call($('<p class=response>').appendTo(self), handler(this.textContent || this.innerText));
					}
					newline();
					return false;
				} else if (code == 9) { // tab
					autocomplete(this, commands);
					setEndOfContenteditable(this);
					e.preventDefault();
				} else if (code == 27) { // escape
					$(this).text('');
				} else if (code == 38) { // down
					$(this).text(nextHistory());
				} else if (code == 40) { // up
					$(this).text(prevHistory());
				}
			});
		});
	};
})(jQuery);
