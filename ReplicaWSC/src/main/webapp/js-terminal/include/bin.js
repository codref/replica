/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var Replica = function(terminal, effect){
	this.terminal = terminal;
	this.effect = effect;
};

Replica.prototype = {
	
	authtoken : '',
	
	// TODO make this configurable 
	resources : '/replica/images/',
	
	wget : function(url) {
		var me = this,
			prefix = 'http://';
		if (url.substr(0, prefix.length) !== prefix)
		{
			url = prefix + url;
		}
		me.echo('opening... ' + url);
		window.open(url);
		return '';
	},
	
    authenticate: function(host, username, password, callback) {
    	var me = this;
    	$.ajax({
			url: host + '/authenticate.action?username=' + username + '&password=' + password,
			dataType : 'json',
			 xhrFields: {
				 withCredentials: true
				 },
			statusCode: {
				404: function() {
					me.echo('Host not found');
				},
    			500: function() {
    				me.echo('Server error');
    			}
			}
		}).done(function(data) {
			if (data.success) {
				me.echo('Server answer: ' + data.message);
				me.echo('Variable <span class="gray">replica.authtoken</span> is ' + data.data['SpcRest-Auth-Token']);
				if (callback != undefined)
					callback(data);
			} else {
				me.echo('Server answer: ' + data.message);
			}
			me.authtoken = data.data['SpcRest-Auth-Token'];
		});
    	return '';
    },
    
    execscript: function(host, scriptName, authtoken, input, callback) {
    	var me = this;
    	$.ajax({
			url: host + '/script/exec.action?name='+ scriptName,
			type: 'POST',
			dataType : 'json',
			data: { 
				inputParameters: JSON.stringify(input)
			},
			headers: {
				'SpcRest-Auth-Token' : authtoken
			},
			statusCode: {
				404: function() {
					me.echo('Host not found');
				},
    			500: function() {
    				me.echo('Server error');
    			}
			}
		}).done(function(data) {
			if (data.success) {
				me.echo('Server answer: ' + data.message);
				if (callback != undefined)
					callback(data);
			} else {
				me.echo('Server answer: ' + data.message);
			}
		});
    	return '';
    },    
    
	echo: function(text) {
		this.effect.call($('<p class=response>').appendTo($(this.terminal, '[contenteditable]')), text);
		return '';
	}
};
