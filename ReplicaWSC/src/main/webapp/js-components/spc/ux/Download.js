/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('spc.ux.Download', {
	extend : 'Ext.Component',
	alias : 'widget.download',
	autoEl : {
		tag : 'iframe',
		cls : 'x-hidden',
		src : Ext.SSL_SECURE_URL
	},
	load : function(config) {
		this.getEl().dom.src = config.url
		+ (config.params ? '?' + Ext.urlEncode(config.params) : '');
	}
});
