/*
 * This file is part of Replica -- Management Console
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Authors: Davide Dal Farra <dalfarra@codref.com>
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Ext.define('spc.ux.InstanceLogWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.instancelogwindow',
	title: 'Instance Logs Viewer',
    closable: true,
    closeAction: 'hide',
    collapsible: false,
    maximizable: true,
    border: false,
    width: 400,
    height: 350,
    minWidth: 300,
    minHeight: 200,    
    layout: {
        type: 'border'
    },
    modal: true,
    isPolling : false,
    message : '',
    
    initComponent : function() {
    	var me = this;
        
        var logPanel = Ext.create('Ext.panel.Panel', {
	        region: 'center',
	        xtype: 'form',
	        layout: 'fit',
	        dockedItems: [{
    	        xtype: 'toolbar',
    	        dock: 'top',
    	        items: [{
					xtype : 'combo',
					id: 'selected-logFile',
					hideLabel: true,
					store : new Ext.data.SimpleStore({
						data : [ 
						  [ 0, _instance_path_ + '.spc.log' ],
						  [ 1, _instance_path_ + '.imports.log' ],
						  [ 999, 'Replica.log' ]
						],
						id : 0,
						fields : [ 'id', 'filename' ]
					}),
					valueField : 'id',
					value: 0,
					allowBlank : true,
					editable : false,
					displayField : 'filename'
				},{
					xtype : 'combo',
					id: 'selected-rows',
					width: 60,
					hideLabel: true,
					store : new Ext.data.SimpleStore({
						data : [ 
						  [ 1, '~100' ],
						  [ 2, '~200' ],
						  [ 3, '~300' ],
						  [ 4, '~400' ],
						  [ 5, '~500' ],
						  [ 6, '~600' ],
						  [ 7, '~700' ],
						  [ 8, '~800' ],
						  [ 9, '~900' ],
						  [ 10, '~1000' ],
						  [ 11, '~2000' ],
						  [ 12, '~5000' ],
						  [ 13, '~10000' ]
						],
						fields : [ 'id', 'rows' ]
					}),
					valueField : 'id',
					value: 4,
					allowBlank : true,
					editable : false,
					displayField : 'rows'
				},{
					text: 'Tail',
			        tooltip: 'Download last n rows of the selected file and show them on the control below',
			        clickEvent: 'mousedown',
			        handler: function(){
			        	Ext.Ajax.request({
			        		url: _instance_path_ + '/log/view.action',
			        	    params: {
			        	    	rows: logPanel.down('#selected-rows').getValue(),
			        	    	logFile: logPanel.down('#selected-logFile').getValue()
			        	    },
			        	    success: function(response, opts) {
			        	    	var result = Ext.decode(response.responseText);
			        	    	if (!result.success) {
			        	    		if (result.message != undefined) 
			        	    			Ext.example.msg('Error',result.message);
			        	    		else 
			        	    			Ext.example.msg('Error','Cannot fetch selected file.');
			        	    		return;
			        	    	}
			        	    	logFileContent = logPanel.down('#log-file-content');
			        	    	logFileContent.setValue(result.data.content);
                    		},
                    		failure: function(response, opts) {
                    			Ext.example.msg('Failure','');
                    		}
			        	});			        	
			        }
				}, '-' ,{
					text: 'Download',
					tooltip: 'Download the whole file on local hard drive',
					clickEvent: 'mousedown',
					handler: function() {
				    	Ext.getCmp('downloader').load({
				    		url: _instance_path_ + '/log/download.action',  
				    		method: 'POST',
				            params  : { 
				            	logFile: logPanel.down('#selected-logFile').getValue()
				            }
				    	});
					}
				}]
	        }],
	        items: [{
	        	xtype: 'codemirror',
	        	id: 'log-file-content',
	            pathModes: '/CodeMirror-2.24/mode',
	            pathExtensions: '/CodeMirror-2.24/lib/util',
	            anchor: '100%',
	            hideLabel: true,
	            showModes: false,
	            readOnly: true,
	            showAutoIndent: false,
	            mode: 'text/javascript'
	        }]
        });
    	
    	Ext.apply(this, {
    	    items: [logPanel]
    	});
    	this.callParent(arguments);
    }
});
