/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.codref.r.replicaws.model.Script;

@Repository
public class ScriptDAO extends BasicDAO {
	
	public List getScriptsCount(String clauses) {
		return sessionFactory.getCurrentSession().createQuery("select count(*) from Script "+clauses).list();
	}	

	@SuppressWarnings("unchecked")
	public List<Script> getScripts() {
		return sessionFactory.getCurrentSession().createQuery("select new Script(id, name, description) from Script").list();
	}

	public Script getScript(int id) {
		return (Script) sessionFactory.getCurrentSession().createQuery("from Script where id = :id")
				.setParameter("id", id)
				.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Script> getScripts(String clauses, int start, int limit) {
		return sessionFactory.getCurrentSession().createQuery("select new Script(id, name, description) from Script "+clauses).setFirstResult(start).setMaxResults(limit).list();
	}	

	public void deleteScript(int id){
		Object record = sessionFactory.getCurrentSession().load(Script.class, id);
		sessionFactory.getCurrentSession().delete(record);
	}
	
	public Script saveScript(Script Script){
		sessionFactory.getCurrentSession().saveOrUpdate(Script);
		return Script;
	}
}