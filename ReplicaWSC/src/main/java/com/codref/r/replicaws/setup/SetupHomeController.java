/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.setup;

import java.security.Principal;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.codref.r.replicaws.service.ConfigService;
import com.codref.r.replicaws.util.SecurityUtil;

/**
 * Controller - Spring
 */
@Controller
public class SetupHomeController  {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private ConfigService configService;

	@SuppressWarnings("unchecked")
	@RequestMapping("/setup/index.action")
	public ModelAndView indexView(HttpServletRequest request, Principal principal,
			 @RequestParam(value = "action", required = false) String action,
			 @RequestParam(value = "id", required = false) Integer id,
			 @RequestParam(value = "code", required = false) String code) throws Exception {
		ModelAndView mav = new ModelAndView();
		Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		
		
		// redirect to correct url accordingly to user roles
		if (SecurityUtil.hasRole(SecurityUtil.ROLE_SETUP, authorities.toString()) || !SecurityUtil.redirect(mav, authorities.toString(), SecurityUtil.GOTO_SETUP)) {
			try {
				mav.addObject("resources_url", configService.getConfigList("resources.url"));
			} catch(Exception e) {
				logger.warn("Instance not configured, resources.url is missing");
				mav.addObject("resources_url", "");
			}
			mav.addObject("instance_name", configService.getConfigList("instance.name"));
			mav.addObject("extjs_debug", configService.getConfigList("extjs.debug"));
			mav.addObject("username",principal.getName());
			mav.addObject("roles", authorities.toString());
		} else {
			logger.warn("User tried to access restricted page");
		}
		
		return mav;
	}
	
}