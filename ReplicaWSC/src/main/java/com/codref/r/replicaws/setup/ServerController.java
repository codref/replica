/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.setup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.client.api.Response;
import org.eclipse.jetty.client.util.InputStreamResponseListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.codref.r.replicaws.model.Server;
import com.codref.r.replicaws.service.ServerService;
import com.codref.r.replicaws.util.GenericUtil;

@Controller
public class ServerController extends BasicController {
	

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private ServerService serverService;
	
	@Autowired
	private GenericUtil genericUtil;
	
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/server/view.action")
	public @ResponseBody Map<String,? extends Object> serverView(
			@RequestParam(required = false) Integer page, 
			@RequestParam(required = false) Integer start, 
			@RequestParam(required = false) Integer limit, 
			@RequestParam(required = false) String search, 
			@RequestParam(required = false) String sort) throws Exception {
		
		try{
			List<Server> servers = serverService.getServerList(page, start, limit, search, sort);
			long count = serverService.getServerCount(search);
			return getMap(servers, count);

		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/server/update.action")
	public @ResponseBody Map<String,? extends Object> serverUpdate(@RequestParam String data){
		
		try{
			return getMap(serverService.serverUpdate(data));
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}	
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/server/create.action")
	public @ResponseBody Map<String,? extends Object> serverCreate(@RequestParam String data){
		
		try{
			return getMap(serverService.serverCreate(data));
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}	
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/server/delete.action")
	public @ResponseBody Map<String,? extends Object> serverDelete(@RequestParam String data){
		
		try{
			serverService.serverDelete(data);
			return getMap();
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}	
	
	@SuppressWarnings("unchecked")
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/server/viewProcessStatus.action")
	public @ResponseBody Map<String,? extends Object> viewProcessesStatus(@RequestParam String address){
		HttpClient httpClient = new HttpClient();
		InputStreamResponseListener listener = new InputStreamResponseListener();
		
		try{
			httpClient.start();
			httpClient.newRequest(String.format("http://%s:9967/replicaProcessStatus", address)).send(listener);
			Response response = listener.get(5, TimeUnit.SECONDS);
			String result = "";
			if (response.getStatus() == 200)
			{
			    InputStream responseContent = listener.getInputStream();
			    result = getStringFromInputStream(responseContent);
			    return getMap((ArrayList<Object>) genericUtil.getObjectFromJSON(result, ArrayList.class));
			} else 
				return getModelMapError("Error");	
		} catch (Exception e) {
			logger.error("Error",e);
			return getModelMapError("Error");
		}
	}
	
	@SuppressWarnings("unchecked")
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/server/viewProcessCount.action")
	public @ResponseBody Map<String,? extends Object> viewProcessesCount(@RequestParam String address){
		HttpClient httpClient = new HttpClient();
		InputStreamResponseListener listener = new InputStreamResponseListener();
		
		try{
			httpClient.start();
			httpClient.newRequest(String.format("http://%s:9967/replicaProcessCount", address)).send(listener);
			Response response = listener.get(5, TimeUnit.SECONDS);
			String result = "";
			if (response.getStatus() == 200)
			{
			    InputStream responseContent = listener.getInputStream();
			    result = getStringFromInputStream(responseContent);
			    return getMap((Integer) genericUtil.getObjectFromJSON(result, Integer.class));
			} else 
				return getModelMapError("Error");	
		} catch (Exception e) {
			logger.error("Error",e);
			return getModelMapError("Error");
		}
	}	
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/server/processKill.action")
	public @ResponseBody Map<String,? extends Object> processKill(@RequestParam String address, @RequestParam Integer pid){
		HttpClient httpClient = new HttpClient();
		InputStreamResponseListener listener = new InputStreamResponseListener();
		
		try{
			httpClient.start();
			httpClient.newRequest(String.format("http://%s:9967/replicaProcessKill?pid=%s", address, pid)).send(listener);
			Response response = listener.get(5, TimeUnit.SECONDS);
			String result = "";
			if (response.getStatus() == 200)
			{
			    InputStream responseContent = listener.getInputStream();
			    result = getStringFromInputStream(responseContent);
			    return getMap((String) genericUtil.getObjectFromJSON(result, String.class));
			} else 
				return getModelMapError("Error");	
		} catch (Exception e) {
			logger.error("Error",e);
			return getModelMapError("Error");
		}
	}	
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/server/processRestart.action")
	public @ResponseBody Map<String,? extends Object> processRestart(@RequestParam String address, @RequestParam String command){
		HttpClient httpClient = new HttpClient();
		InputStreamResponseListener listener = new InputStreamResponseListener();
		
		try{
			httpClient.start();
			httpClient.newRequest(String.format("http://%s:9967/replicaProcessRestart?startCommand=%s", address, command)).send(listener);
			Response response = listener.get(5, TimeUnit.SECONDS);
			String result = "";
			if (response.getStatus() == 200)
			{
			    InputStream responseContent = listener.getInputStream();
			    result = getStringFromInputStream(responseContent);
			    return getMap((String) genericUtil.getObjectFromJSON(result, String.class));
			} else 
				return getModelMapError("Error");	
		} catch (Exception e) {
			logger.error("Error",e);
			return getModelMapError("Error");
		}
	}		
	
	private static String getStringFromInputStream(InputStream is) {
		 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
 
	}
	
}