/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.codref.r.replicaws.model.Config;

@Repository
public class ConfigDAO extends BasicDAO {
	
	public List getConfigsCount(String clauses) {
		return sessionFactory.getCurrentSession().createQuery("select count(*) from Config "+clauses).list();
	}	

	@SuppressWarnings("unchecked")
	public List<Config> getConfigs() {
		return sessionFactory.getCurrentSession().createQuery("from Config").list();
	}

	@SuppressWarnings("unchecked")
	public List<Config> getConfigs(int id) {
		return sessionFactory.getCurrentSession().createQuery("from Config where id = :id")
				.setParameter("id", id)
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Config> getConfigs(String label) {
		return sessionFactory.getCurrentSession().createQuery("from Config where label = :label")
				.setParameter("label", label)
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Config> getConfigs(String clauses, int start, int limit) {
		return sessionFactory.getCurrentSession().createQuery("from Config "+clauses).setFirstResult(start).setMaxResults(limit).list();
	}	

	public void deleteConfig(int id){
		Object record = sessionFactory.getCurrentSession().load(Config.class, id);
		sessionFactory.getCurrentSession().delete(record);
	}
	
	public Config saveConfig(Config Config){
		sessionFactory.getCurrentSession().saveOrUpdate(Config);
		return Config;
	}
}