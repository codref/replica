/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.util;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Component;

import com.codref.r.replicaws.model.Search;
import com.codref.r.replicaws.model.Sort;

@Component
public class GenericUtil {
	

	public <T> T getObjectFromJSON(String jsonObject, Class<T> type) throws JsonParseException, JsonMappingException, IOException{
		return  type.cast(new ObjectMapper().readValue(jsonObject, type));
	}	
	
	public <T> Integer getIdFromJSON(String jsonObject, Class<T> type) throws JsonParseException, JsonMappingException, IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		T obj = getObjectFromJSON(jsonObject, type);
		Method getId = type.getMethod("getId", (Class[])null); 
		return (Integer) getId.invoke(obj, new Object[] {});
	}	
	
	public String getWhere(String search) throws JsonParseException, JsonMappingException, IOException{
		String query = new String();
		if (search != null){
			Search newSearch = new ObjectMapper().readValue(search, Search.class);
			if (!newSearch.getFields().isEmpty()){
				query = " where ";
				Iterator fields = newSearch.getFields().iterator();
				String field = (String)fields.next();
				query = query+field+" like '%"+newSearch.getQuery()+"%' OR ";
				while (fields.hasNext()){
					field = (String)fields.next();
					query = query+field+" like '%"+newSearch.getQuery()+"%' OR ";
				}
				query = query.substring(0, query.lastIndexOf(" OR ")).trim();
				query = " "+query;
			}
		}
		return query;
	}
	
	public String getWhereOrder(String search, String sort) throws JsonParseException, JsonMappingException, IOException{
		String query = new String();
		if (search != null){
			Search newSearch = new ObjectMapper().readValue(search, Search.class);
			if (!newSearch.getFields().isEmpty()){
				query = " where ";
				Iterator fields = newSearch.getFields().iterator();
				String field = (String)fields.next();
				query = query+field+" like '%"+newSearch.getQuery()+"%' OR ";
				while (fields.hasNext()){
					field = (String)fields.next();
					query = query+field+" like '%"+newSearch.getQuery()+"%' OR ";
				}
				query = query.substring(0, query.lastIndexOf(" OR ")).trim();
				query = " "+query;
			}
		}
		if (sort != null){
			List<Sort> newSorts = new ObjectMapper().readValue(sort, new TypeReference<List<Sort>>() {} );
			if (!newSorts.isEmpty()){
				query = query+" order by ";
				Iterator sorts = newSorts.iterator();
				while (sorts.hasNext()){
					Sort sortObj = (Sort)sorts.next();
					query = query+sortObj.getProperty()+" "+sortObj.getDirection()+" ";
				}
			}
		}
		return query;
	}	
}