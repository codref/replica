/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.service;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.springframework.stereotype.Service;

import com.codref.r.replica.Replica;
import com.codref.r.replica.ReplicaException;

@Service
public class ParameterService {

	public void fromJson(Replica replica, String jsonEncodedParameters) throws JsonProcessingException, IOException, ReplicaException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.readTree(jsonEncodedParameters);
		Iterator<JsonNode> i = rootNode.getElements();
		while (i.hasNext()) {
			JsonNode param = i.next();
			String name = param.path("name").getTextValue();
			String type = param.path("type").getTextValue();
			if (type.equals("int")) {
				JsonNode value = param.path("value");
				replica.assignParameter(name, value.getIntValue());
			} else if (type.equals("int[]")) {
				JsonNode values = param.path("value");
				Iterator<JsonNode> j = values.getElements();
				int[] array = new int[values.size()];
				int c = 0;
				while (j.hasNext()) {
					JsonNode value = j.next();
					array[c] = value.getIntValue();
					c++;
				}
				replica.assignParameter(name, array);
			} else if (type.equals("double")) {
				JsonNode value = param.path("value");
				replica.assignParameter(name, value.getDoubleValue());
			} else if (type.equals("double[]")) {
				JsonNode values = param.path("value");
				Iterator<JsonNode> j = values.getElements();
				double[] array = new double[values.size()];
				int c = 0;
				while (j.hasNext()) {
					JsonNode value = j.next();
					array[c] = value.getDoubleValue();
					c++;
				}
				replica.assignParameter(name, array);
			} else if (type.equals("string")) {
				JsonNode value = param.path("value");
				replica.assignParameter(name, value.getTextValue());
			} else if (type.equals("string[]")) {
				JsonNode values = param.path("value");
				Iterator<JsonNode> j = values.getElements();
				String[] array = new String[values.size()];
				int c = 0;
				while (j.hasNext()) {
					JsonNode value = j.next();
					array[c] = value.getTextValue();
					c++;
				}
				replica.assignParameter(name, array);
			}
		}
	}
	
	public ObjectNode toJson(Map<String, REXP> outputs) throws REXPMismatchException {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode root = mapper.createObjectNode();
		for (Map.Entry<String, REXP> variable : outputs.entrySet()) {
		    String name = variable.getKey();
		    REXP rexp = variable.getValue();
		    ObjectNode child = mapper.createObjectNode();
		    
		    if (rexp.isVector()) {
		    	// double
		    	if (rexp.isNumeric()) {
		    		if (rexp.length() > 1) {
		    			child.put("type", "double[]");
		    			ArrayNode jvalues= mapper.createArrayNode();
		    			for (double d : rexp.asDoubles()) {
							jvalues.add(d);
						}
		    			child.put("value", jvalues);
		    		} else {
		    			child.put("type", "double");
		    			child.put("value", rexp.asDouble());
		    		}
		    	// string
		    	} else if (rexp.isString()) {
		    		if (rexp.length() > 1) {
		    			child.put("type", "string[]");
		    			ArrayNode jvalues= mapper.createArrayNode();
		    			for (String d : rexp.asStrings()) {
							jvalues.add(d);
						}
		    			child.put("value", jvalues);		    			
		    		} else {
		    			child.put("type", "string");
		    			child.put("value", rexp.asString());		    			
		    		}
		    	// int
		    	} else if (rexp.isInteger()) {
		    		if (rexp.length() > 1) {
		    			child.put("type", "int[]");
		    			ArrayNode jvalues= mapper.createArrayNode();
		    			for (int d : rexp.asIntegers()) {
							jvalues.add(d);
						}
		    			child.put("value", jvalues);		    			
		    		} else {
		    			child.put("type", "int");
		    			child.put("value", rexp.asInteger());		    			
		    		}
		    	} 
		    }
		    
		    root.put(name, child);
		}
			
		return root;
	}
}
