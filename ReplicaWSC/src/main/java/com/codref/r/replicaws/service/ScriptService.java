/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codref.r.replicaws.dao.OutputDAO;
import com.codref.r.replicaws.dao.ScriptDAO;
import com.codref.r.replicaws.model.Output;
import com.codref.r.replicaws.model.Script;
import com.codref.r.replicaws.util.GenericUtil;

@Service
public class ScriptService {

	@Autowired	
	private ScriptDAO scriptDAO;
	
	@Autowired	
	private OutputDAO outputDAO;
	
	@Autowired
	private GenericUtil genericUtil;
	
	@Autowired
	private ReplicaCacheService replicaCacheService;
	
	
	@Transactional
	public void delete(String data) throws JsonParseException, JsonMappingException, IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Script script = genericUtil.getObjectFromJSON(data, Script.class);
		outputDAO.deleteScriptOutput(script.getId());
		scriptDAO.deleteScript(script.getId());
		replicaCacheService.delScript(script.getName());
		replicaCacheService.delScriptOutput(script.getId());
	}	

	@Transactional
	public Script update(Script script) throws JsonParseException, JsonMappingException, IOException{
		Script result = scriptDAO.saveScript(script);
		replicaCacheService.delScript(result.getName());
		return result;
	}	
	
	@Transactional
	public Script create(Script script) throws JsonParseException, JsonMappingException, IOException{
		Script result = scriptDAO.saveScript(script); 
		replicaCacheService.delScript(result.getName());
		replicaCacheService.delScriptOutput(result.getId());
		return result;
	}	
	
	@Transactional(readOnly=true)
	public long getScriptCount(String search) throws JsonParseException, JsonMappingException, IOException{
		String query = genericUtil.getWhere(search);
		List count = scriptDAO.getScriptsCount(query);
		Long countLong = (Long)count.get(0);
		return countLong;
	}	
	
	@Transactional(readOnly=true)
	public List<Script> getScriptList(Integer page, Integer start, Integer limit, String search, String sort){
		try {
			int startInt = start;
			int limitInt = limit;
			String query = genericUtil.getWhereOrder(search, sort);
			return scriptDAO.getScripts(query, startInt, limitInt);
		} catch (Exception e) {
			return scriptDAO.getScripts();
		}
	}	

	@Transactional(readOnly=true)
	public List<Script> getScriptList(){
		List<Script> Scripts = scriptDAO.getScripts();
		return Scripts;
	}
	
	@Transactional(readOnly=true)
	public Script getSingleScript(Integer scriptId){
		return scriptDAO.getScript(scriptId);
	}	
	
	
	/*
	 * Output
	 * 
	 */
	
	private String addScriptIdToQuery(Integer scriptId, String query) {
		if (query.indexOf("where") > 0) {
			return query + " and script = " + scriptId;
		} else {
			return " where script = " + scriptId;
		}
	}
	
	@Transactional(readOnly=true)
	public List<Output> getOutputList(Integer scriptId, Integer page, Integer start, Integer limit, String search, String sort) throws JsonParseException, JsonMappingException, IOException{
		int startInt = start;
		int limitInt = limit;
		String clause = genericUtil.getWhereOrder(search, sort);
		return outputDAO.getOutputs(addScriptIdToQuery(scriptId,clause), startInt, limitInt);
	}	
	
	@Transactional(readOnly=true)
	public long getOutputCount(Integer scriptId, String search) throws JsonParseException, JsonMappingException, IOException{
		String clause = genericUtil.getWhere(search);
		return outputDAO.getOutputsCount(addScriptIdToQuery(scriptId,clause));
	}	
	
	@Transactional
	public Output outputUpdate(String data) throws JsonParseException, JsonMappingException, IOException{
		Output output = genericUtil.getObjectFromJSON(data, Output.class);
		replicaCacheService.delScriptOutput(output.getScript());
		return outputDAO.saveOutput(output);
	}
	
	@Transactional
	public Output outputCreate(String data) throws JsonParseException, JsonMappingException, IOException{
		Output output = genericUtil.getObjectFromJSON(data, Output.class);
		replicaCacheService.delScriptOutput(output.getScript());
		return outputDAO.saveOutput(output);
	}	
	
	@Transactional
	public void outputDelete(String data) throws JsonParseException, JsonMappingException, IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Output output = genericUtil.getObjectFromJSON(data, Output.class);
		replicaCacheService.delScriptOutput(output.getScript());		
		outputDAO.deleteOutput(output.getId());
	}

}
