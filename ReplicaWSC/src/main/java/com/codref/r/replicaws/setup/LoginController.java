/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.setup;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.codref.r.replicaws.service.ConfigService;
 
@Controller
public class LoginController {
	
	// TODO enhance log details
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private ConfigService configService;
	
	@RequestMapping(value="/login.action", method = RequestMethod.GET)
	public ModelAndView login(HttpServletRequest request, Principal principal){
		ModelAndView mav = new ModelAndView();
		try {
			mav.addObject("resources_url", configService.getConfigList("resources.url"));
		} catch(Exception e) {
			logger.warn("Instance not configured, resources.url is missing");
			mav.addObject("resources_url", "");
		}
		return mav;
 	}
 
	@RequestMapping(value="/loginfailed.action", method = RequestMethod.GET)
	public ModelAndView loginerror(HttpServletRequest request, Principal principal){
		logger.warn("Login attempt failed!");
		ModelAndView mav = new ModelAndView();
		try {
			mav.addObject("resources_url", configService.getConfigList("resources.url"));
		} catch(Exception e) {
			logger.warn("Instance not configured, resources.url is missing");
			mav.addObject("resources_url", "");
		}
		mav.addObject("error", "true");
		return mav;
 
	}
 
	@RequestMapping(value="/logout.action", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request, Principal principal){
		logger.info("User logged out");
		ModelAndView mav = new ModelAndView();
		try {
			mav.addObject("resources_url", configService.getConfigList("resources.url"));
		} catch(Exception e) {
			logger.warn("Instance not configured, resources.url is missing");
			mav.addObject("resources_url", "");
		}
		return mav;
 
	}
 
}