/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.setup;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.codref.r.replicaws.model.Output;
import com.codref.r.replicaws.model.Script;
import com.codref.r.replicaws.service.ScriptService;

@Controller
public class ScriptController extends BasicController {
	

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private ScriptService scriptService;
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/script/view.action")
	public @ResponseBody Map<String,? extends Object> view(
			@RequestParam(required = false) Integer page, 
			@RequestParam(required = false) Integer start, 
			@RequestParam(required = false) Integer limit, 
			@RequestParam(required = false) String search, 
			@RequestParam(required = false) String sort){
		
		try{

			List<Script> scripts = scriptService.getScriptList(page, start, limit, search, sort);
			long count = scriptService.getScriptCount(search);
			return getMap(scripts, count);

		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/script/viewSingleScript.action")
	public @ResponseBody Map<String,? extends Object> viewSingleScript(
			@RequestParam(required = true) Integer scriptId){
		try{
			return getMap(scriptService.getSingleScript(scriptId));
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
	
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/script/create.action")
	public @ResponseBody Map<String,? extends Object> create(
			@RequestParam Integer id,
			@RequestParam String name,
			@RequestParam String content,
			@RequestParam String description){

		Script script = new Script();
		script.setId(id);
		script.setName(name);
		script.setContent(content);
		script.setDescription(description);
		try{
			return getMap(scriptService.create(script));
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/script/update.action")
	public @ResponseBody Map<String,? extends Object> update(
			@RequestParam Integer id,
			@RequestParam String name,
			@RequestParam String content,
			@RequestParam String description){
		
		Script script = new Script();
		script.setId(id);
		script.setName(name);
		script.setContent(content);
		script.setDescription(description);
		try{
			return getMap(scriptService.update(script));
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/script/delete.action")
	public @ResponseBody Map<String,? extends Object> delete(@RequestParam String data){
		
		try{
			scriptService.delete(data);
			return getMap();
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
	
	
	/*
	 * Output
	 * 
	 */
	
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/script/output/view.action")
	public @ResponseBody Map<String,? extends Object> outputView(
			@RequestParam(required = true) Integer scriptId, 
			@RequestParam(required = false) Integer page, 
			@RequestParam(required = false) Integer start, 
			@RequestParam(required = false) Integer limit, 
			@RequestParam(required = false) String search, 
			@RequestParam(required = false) String sort) throws Exception {
		
		try{
			List<Output> outputs = scriptService.getOutputList(scriptId, page, start, limit, search, sort);
			long count = scriptService.getOutputCount(scriptId, search);
			return getMap(outputs, count);

		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/script/output/update.action")
	public @ResponseBody Map<String,? extends Object> outputUpdate(@RequestParam String data){
		
		try{
			return getMap(scriptService.outputUpdate(data));
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}	
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/script/output/create.action")
	public @ResponseBody Map<String,? extends Object> outputCreate(@RequestParam String data){
		
		try{
			return getMap(scriptService.outputCreate(data));
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}	
	
	@Secured({"ROLE_SETUP"})
	@RequestMapping(value="/setup/script/output/delete.action")
	public @ResponseBody Map<String,? extends Object> outputDelete(@RequestParam String data){
		
		try{
			scriptService.outputDelete(data);
			return getMap();
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}	
	
}