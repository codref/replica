/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.codref.r.replicaws.model.Server;

@Repository
public class ServerDAO extends BasicDAO {
	
	public Long getServersCount(String clauses) {
		List count = sessionFactory.getCurrentSession().createQuery("select count(*) from Server "+clauses).list();
		return (Long)count.get(0);
	}	

	@SuppressWarnings("unchecked")
	public List<Server> getServers() {
		return sessionFactory.getCurrentSession().createQuery("from Server").list();
	}

	@SuppressWarnings("unchecked")
	public List<Server> getServer(int id) {
		return sessionFactory.getCurrentSession().createQuery("from Server where id = :id")
				.setParameter("id", id)
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Server> getServers(String clauses, int start, int limit) {
		return sessionFactory.getCurrentSession().createQuery("from Server "+clauses).setFirstResult(start).setMaxResults(limit).list();
	}	

	public void deleteServer(int id){
		Object record = sessionFactory.getCurrentSession().load(Server.class, id);
		sessionFactory.getCurrentSession().delete(record);
	}
	
	public Server saveServer(Server user){
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		return user;
	}
}