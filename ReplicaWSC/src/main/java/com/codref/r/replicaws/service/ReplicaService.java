/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.service;

import java.sql.SQLException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import redis.clients.jedis.Jedis;

import com.codref.r.replica.Replica;
import com.codref.r.replica.ReplicaCache;
import com.codref.r.replica.ReplicaException;

@Service
public class ReplicaService {
	
	@Autowired
	private ReplicaCacheService replicaCacheService;
	
	@Value("${replica.db.url}")
	private String replicaDB;
	
	@Value("${replica.db.driver}")
	private String replicaDriver;
	
	@Value("${replica.db.prefix}")
	private String replicaPrefix;
	
	@Value("${redist.instance.id}")
	private String redisInstanceId;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private HashMap<String,Replica> instances = new HashMap<>();
	
	public Replica getNewReplicaInstance() throws Exception {
		ReplicaCache replicaCache = new ReplicaCache(replicaCacheService.getConnection(), redisInstanceId);
		Replica replicaInstance = new Replica(replicaDB, replicaDriver, replicaPrefix, replicaCache); 
		return replicaInstance;
	}
	
	public Replica getNewReplicaInstance(String sessionId) throws Exception {
		ReplicaCache replicaCache = new ReplicaCache(replicaCacheService.getConnection(), redisInstanceId,  sessionId);
		Replica replicaInstance = new Replica(replicaDB, replicaDriver, replicaPrefix, replicaCache); 
		instances.put(sessionId, replicaInstance);
		return replicaInstance;
	}
	
	public Replica getReplicaInstance(String sessionId) {
		return instances.get(sessionId);
	}	
	
	public boolean dropReplicaInstance(Replica instance) throws SQLException {
		if (instance != null) {
			replicaCacheService.releaseConnection(((ReplicaCache) instance.getCacheInstance()).getJedisInstance());
			return instance.close();
		} else {
			logger.error("Cannot drop an empty replica instance");
		}
		return false;
	}
	
	public boolean dropReplicaInstance(String sessionId) throws SQLException {
		Replica instance = instances.get(sessionId);
		if (instance != null) {
			replicaCacheService.releaseConnection((Jedis) instance.getCacheInstance());
			instances.remove(sessionId);
			logger.info("closing connection ["+sessionId+"]");
			return instance.close();
		} else {
			logger.error("cannot find sessionId and close connection");
		}
		return false;
	}
}
