/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codref.r.replicaws.dao.UserDAO;
import com.codref.r.replicaws.model.User;
import com.codref.r.replicaws.model.UserRoles;
import com.codref.r.replicaws.util.GenericUtil;

@Service
public class UserService {

	@Autowired	
	private UserDAO userDAO;
	
	@Autowired
	private GenericUtil genericUtil;
	
	
	@Transactional
	public void delete(String data) throws JsonParseException, JsonMappingException, IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Integer id = genericUtil.getIdFromJSON(data, User.class);
		userDAO.deleteUser(id);
	}	

	@Transactional
	public User update(User user) {
		User newUser = userDAO.saveUser(user);
		userDAO.deleteRoles(user.getId());
		for (String role : user.getRoles()) {
			UserRoles newRole = new UserRoles();
			newRole.setUser_id(newUser.getId());
			newRole.setAuthority(role);
			userDAO.saveRoles(newRole);
		}
		return newUser;
	}	
	
	@Transactional
	public User create(User user) throws JsonParseException, JsonMappingException, IOException{
		User newUser = userDAO.saveUser(user);
		for (String role : user.getRoles()) {
			UserRoles newRole = new UserRoles();
			newRole.setUser_id(newUser.getId());
			newRole.setAuthority(role);
			userDAO.saveRoles(newRole);
		}
		return newUser;
	}	
	
	@Transactional(readOnly=true)
	public long getUserCount(String search) throws JsonParseException, JsonMappingException, IOException{
		String query = genericUtil.getWhere(search);
		List count = userDAO.getUsersCount(query);
		Long countLong = (Long)count.get(0);
		return countLong;
	}	
	
	@Transactional(readOnly=true)
	public List<User> getUserList(Object page, Integer start, Integer limit, String search, String sort){
		try {
			int startInt = start;
			int limitInt = limit;
			String query = genericUtil.getWhereOrder(search, sort);
			return userDAO.getUsers(query, startInt, limitInt);
		} catch (Exception e) {
			return userDAO.getUsers();
		}
	}	

	@Transactional(readOnly=true)
	public List<User> getUserList(){
		return userDAO.getUsers();
	}
	
	@Transactional(readOnly=true)
	public List<String> getUserRoles(Integer userId) {
		List<String> roles = new ArrayList<>();
		for (UserRoles role : userDAO.getRoles(userId)) {
			roles.add(role.getAuthority());
		}
		return roles;
	}		
	
}
