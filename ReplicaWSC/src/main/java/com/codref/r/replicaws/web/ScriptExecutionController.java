/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonProcessingException;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.codref.r.replica.Replica;
import com.codref.r.replica.ReplicaException;
import com.codref.r.replicaws.model.RestSession;
import com.codref.r.replicaws.service.AuthenticationService;
import com.codref.r.replicaws.service.ParameterService;
import com.codref.r.replicaws.service.ReplicaService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/script")
public class ScriptExecutionController {

	@Autowired
	ReplicaService replicaService;
	
	@Autowired
	AuthenticationService authenticationService;
	
	@Autowired
	ParameterService parameterService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping("exec.action")
	@ResponseBody
	public Map<String, Object> exec(HttpServletRequest request, HttpServletResponse response, @RequestParam String name, @RequestParam String inputParameters) throws Exception {	
		
		RestSession session = authenticationService.isAuthenticated(request);
		
		if  (session == null) {
			logger.warn("Not authenticated or session is expired");
			return getModelMapError("Not authenticated or session is expired");
		}
			
		Replica replica = replicaService.getNewReplicaInstance();
		
		try {
		
			replica.connect();
			parameterService.fromJson(replica, inputParameters);
			
			Map<String, REXP> res = replica.execute(name);		
			
			return getModelMap("Done", parameterService.toJson(res));
			
		} catch (ReplicaException e) {
			logger.error(e.getMessage(), e);
			return getModelMapError(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			return getModelMapError("Configuration error. " + e.getMessage());
		} catch (REXPMismatchException e) {
			logger.error(e.getMessage(), e);
			return getModelMapError("Parameter fetch error. " + e.getMessage());
		}
		finally {
			boolean dropped = replicaService.dropReplicaInstance(replica);
			logger.info("dropped connection: " + dropped);
		}
	}
	
	
	private Map<String,Object> getModelMap(String msg, Object data){

		Map<String,Object> modelMap = new HashMap<String,Object>(3);
		modelMap.put("message", msg);
		modelMap.put("data", data);
		modelMap.put("success", true);

		return modelMap;
	} 	
	
	private Map<String,Object> getModelMapError(String msg){

		Map<String,Object> modelMap = new HashMap<String,Object>(3);
		modelMap.put("message", msg);
		modelMap.put("success", false);

		return modelMap;
	} 		
}
