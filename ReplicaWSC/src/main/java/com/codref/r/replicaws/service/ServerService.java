/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codref.r.replicaws.dao.ServerDAO;
import com.codref.r.replicaws.model.Server;
import com.codref.r.replicaws.util.GenericUtil;

@Service
public class ServerService {
	
	@Autowired	
	private ServerDAO serverDAO;
	
	@Autowired
	private GenericUtil genericUtil;
	
	@Autowired
	private ReplicaCacheService replicaCacheService;	
	
	@Transactional(readOnly=true)
	public List<Server> getServerList(Integer page, Integer start, Integer limit, String search, String sort) throws JsonParseException, JsonMappingException, IOException{
		int startInt = start;
		int limitInt = limit;
		String clause = genericUtil.getWhereOrder(search, sort);
		return serverDAO.getServers(clause, startInt, limitInt);
	}	
	
	@Transactional(readOnly=true)
	public long getServerCount(String search) throws JsonParseException, JsonMappingException, IOException{
		String clause = genericUtil.getWhere(search);
		return serverDAO.getServersCount(clause);
	}	
	
	@Transactional
	public Server serverUpdate(String data) throws JsonParseException, JsonMappingException, IOException{
		Server server = genericUtil.getObjectFromJSON(data, Server.class);
		replicaCacheService.delServerList();
		return serverDAO.saveServer(server);
	}
	
	@Transactional
	public Server serverCreate(String data) throws JsonParseException, JsonMappingException, IOException{
		Server server = genericUtil.getObjectFromJSON(data, Server.class);
		replicaCacheService.delServerList();
		return serverDAO.saveServer(server);
	}	
	
	@Transactional
	public void serverDelete(String data) throws JsonParseException, JsonMappingException, IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Integer id = genericUtil.getIdFromJSON(data, Server.class);
		replicaCacheService.delServerList();
		serverDAO.deleteServer(id);
	}

}
