/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.setup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BasicController {
	
	protected <T> Map<String,Object> getMap(T result){
		List<T> results = new ArrayList<>();
		results.add(result);
		return getMap(results);
	}
	
	protected <T> Map<String,Object> getMap(List<T> results){
		
		Map<String,Object> modelMap = new HashMap<String,Object>(3);
		modelMap.put("total", results.size());
		modelMap.put("data", results);
		modelMap.put("success", true);
		
		return modelMap;
	}
	
	protected <T> Map<String,Object> getMap(List<T> results, long total){
		
		Map<String,Object> modelMap = new HashMap<String,Object>(3);
		modelMap.put("total", total);
		modelMap.put("data", results);
		modelMap.put("success", true);
		
		return modelMap;
	}
	
	protected <T> Map<String,Object> getRawDataDetailMap(List<T> results, int thereIsCauseAction, long total){
		
		Map<String,Object> modelMap = new HashMap<String,Object>(3);
		modelMap.put("total", total);
		modelMap.put("data", results);
		modelMap.put("causeaction", thereIsCauseAction);
		modelMap.put("success", true);
		
		return modelMap;
	}
	
	protected Map<String,Object> getModelMapError(String msg){

		Map<String,Object> modelMap = new HashMap<String,Object>(2);
		modelMap.put("message", msg);
		modelMap.put("success", false);

		return modelMap;
	} 
	
	protected Map<String,Object> getModelMapSuccess(String msg){

		Map<String,Object> modelMap = new HashMap<String,Object>(2);
		modelMap.put("message", msg);
		modelMap.put("success", true);

		return modelMap;
	} 	
	
	protected Map<String,Object> getModelMap(String msg, Object data){

		Map<String,Object> modelMap = new HashMap<String,Object>(3);
		modelMap.put("message", msg);
		modelMap.put("data", data);
		modelMap.put("success", true);

		return modelMap;
	} 	
	
	protected Map<String,Object> getMap(Map<String,Object> rawdatas){
		
		Map<String,Object> modelMap = new HashMap<String,Object>();
		modelMap.put("data", rawdatas);
		modelMap.put("success", true);
		
		return modelMap;
	}	
	
	protected Map<String,Object> getMap(int count){
		
		Map<String,Object> modelMap = new HashMap<String,Object>(2);
		modelMap.put("count", count);
		modelMap.put("success", true);
		
		return modelMap;
	}
	
	protected Map<String,Object> getMap(){
		
		Map<String,Object> modelMap = new HashMap<String,Object>(1);
		modelMap.put("success", true);
		
		return modelMap;
	}
}