/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.setup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.codref.r.replicaws.model.User;
import com.codref.r.replicaws.service.UserService;

/**
 * Controller - Spring
 */
@Controller
public class UserController extends BasicController  {
	
	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/setup/user/view.action")
	public @ResponseBody Map<String,? extends Object> view(
			@RequestParam(value = "page", required = false) Object page, 
			@RequestParam(value = "start", required = false) Integer start, 
			@RequestParam(value = "limit", required = false) Integer limit, 
			@RequestParam(value = "search", required = false) String search, 
			@RequestParam(value = "sort", required = false) String sort) throws Exception {
		
		try{
			List<User> users = userService.getUserList(page, start, limit, search, sort);
			long count = userService.getUserCount(search);
			return getMap(users, count);
		} catch (Exception e) {
			logger.error("", e);
			return getModelMapError("");
		}
	}
	
	@RequestMapping(value="/setup/user/viewRoles.action")
	public @ResponseBody Map<String,? extends Object> viewUserRoles(@RequestParam Integer userId) throws Exception {
		try{
			return getMap(userService.getUserRoles(userId));
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
	
	@RequestMapping(value="/setup/user/create.action")
	public @ResponseBody Map<String,? extends Object> create(
			@RequestParam String username,
			@RequestParam String plain_password,
			@RequestParam String enabled,
			@RequestParam String[] roles) throws Exception {
		
		User user = new User();
		StandardPasswordEncoder encoder = new StandardPasswordEncoder();
		try{
			user.setUsername(username);
			user.setPassword(encoder.encode(plain_password));
			user.setEnabled(enabled.equals("on") ? 1 : 0);
			user.setRoles(roles);
			return getMap(userService.create(user));
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
	
	@RequestMapping(value="/setup/user/update.action")
	public @ResponseBody Map<String,? extends Object> update(
			@RequestParam Integer id,
			@RequestParam String username,
			@RequestParam String plain_password,
			@RequestParam String password,
			@RequestParam String enabled,
			@RequestParam String[] roles) throws Exception {
		
		User user = new User();
		try{
			user.setId(id);
			user.setUsername(username);
			if (!plain_password.equals("")) {
				StandardPasswordEncoder encoder = new StandardPasswordEncoder();
				user.setPassword(encoder.encode(plain_password));
			} else {
				user.setPassword(password);
			}
			user.setEnabled(enabled.equals("on") ? 1 : 0);
			user.setRoles(roles);
			return getMap(userService.update(user));
		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
	
	@RequestMapping(value="/setup/user/delete.action")
	public @ResponseBody Map<String,? extends Object> delete(@RequestParam String data) throws Exception {
		
		try{
			userService.delete(data);
			Map<String,Object> modelMap = new HashMap<String,Object>(3);
			modelMap.put("success", true);
			return modelMap;

		} catch (Exception e) {
			logger.error("",e);
			return getModelMapError("");
		}
	}
}