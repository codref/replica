/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.service;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codref.r.replicaws.dao.RestSessionsDAO;
import com.codref.r.replicaws.dao.UserDAO;
import com.codref.r.replicaws.model.RestSession;

import org.springframework.security.crypto.password.StandardPasswordEncoder;

@Service
public class AuthenticationService {

	// TODO avoid static values here
	private static final int DEFAULT_KEEPALIVE_TIME = 1800;
	private static final String SPC_REST_AUTH_TOKEN = "SpcRest-Auth-Token";
	
	@Autowired
	private RestSessionsDAO restSessionsDao;
	
	@Autowired
	private UserDAO userDAO;
	
	@Transactional(readOnly=true)
	public RestSession isAuthenticated(HttpServletRequest request) {
		String oldToken = getAuthenticationToken(request); 
		if (oldToken == null)
			return null;
		else return isAuthenticated(oldToken);
	}
	
	public String getAuthenticationToken(HttpServletRequest request) {
		return request.getHeader(SPC_REST_AUTH_TOKEN);
	}
	
	@Transactional(readOnly=true)
	public RestSession isAuthenticated(String token) {
		List<RestSession> sessions = restSessionsDao.getSessionByToken(token);
		if (sessions.isEmpty())
			return null;
		else {
			RestSession session = sessions.get(0);
			if (session.getKeepalive().compareTo(new Timestamp(System.currentTimeMillis())) <= 0)
				return null;
			return session;
		}
	}
	
	@Transactional
	public RestSession createToken() {
		RestSession session = new RestSession();
		Timestamp created = new Timestamp(System.currentTimeMillis());
		Timestamp keepalive = new Timestamp(created.getTime() + DEFAULT_KEEPALIVE_TIME * 1000);
		
		session.setCreated(created);
		session.setKeepalive(keepalive);
		session.setToken(RandomStringUtils.randomAlphanumeric(128));
		
		return restSessionsDao.saveRestSession(session);		
	}

	@Transactional
	public RestSession renewToken(RestSession session) {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		Timestamp keepalive = new Timestamp(now.getTime() + DEFAULT_KEEPALIVE_TIME * 1000);
		session.setKeepalive(keepalive);
		return restSessionsDao.saveRestSession(session);
	}
	
	@Transactional
	public boolean authenticateUser(String username, String password) throws Exception {
		String encodedPassword = userDAO.getEncodedPassword(username);
		
		if (encodedPassword == null)
			return false;
		
		StandardPasswordEncoder encoder = new StandardPasswordEncoder();
		return encoder.matches(password, encodedPassword);
		
	}
}
