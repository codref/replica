package com.codref.r.replicaws.service;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codref.r.replica.Constants;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

@Service
public class ReplicaCacheService {

	private Logger logger = Logger.getLogger(this.getClass());

	private static final String DELETE_SCRIPT_IN_LUA = "local keys = redis.call('keys', '%s')"
			+ "  for i,k in ipairs(keys) do"
			+ "    local res = redis.call('del', k)" + "  end";

	private JedisPool jedisPool;

	@Value("${redist.host}")
	private String redisHost;

	@Value("${redist.port}")
	private int redisPort;
	
	@Value("${redist.instance.id}")
	private String redisInstanceId;	

	@PostConstruct
	private void init() {
		try {
			jedisPool = new JedisPool(new JedisPoolConfig(), redisHost,
					redisPort);
		} catch (Exception e) {
			logger.error("Cannot instantiate Redis server connection pool.", e);
		}
	}

	public Jedis getConnection() throws Exception {
		Jedis jedis = jedisPool.getResource();
		if (jedis == null) {
			logger.error("Unable to get jedis resource.");
			throw new Exception("Unable to get jedis resource!");
		}
		return jedis;
	}
	
	public void releaseConnection(Jedis jedis) {
		if (jedis != null) {
			jedisPool.returnResource(jedis);
		}
	}
	
	public void delServerList() {
		dels("server.*");
	}
	
	public void delScript(String name) {
		dels("script." + name + ".*");
	}
	
	public void delScriptOutput(int scriptId) {
		dels("script." + scriptId + ".output.*");
	}
	
	public void del(String key) {
		Jedis jedis = null;
		try {
			jedis = getConnection();
			jedis.del(String.format(Constants.CACHE_KEY_INSTANCE,redisInstanceId,key));
		} catch (JedisConnectionException e) { 
			if (jedis != null)
				jedisPool.returnBrokenResource(jedis);
			logger.error("Cannot delete requested key due to connection pool error.", e);
			throw new RuntimeException("Unable to delete the requested key!");
		} catch (Exception e) {
			logger.error("Cannot delete requested key", e);
			throw new RuntimeException("Unable to delete the requested key!");
		} finally {
			releaseConnection(jedis);
		}		
	}

	public void dels(String pattern) {
		Jedis jedis = null;
		try {
			jedis = getConnection();
			jedis.eval(String.format(DELETE_SCRIPT_IN_LUA, String.format(Constants.CACHE_KEY_INSTANCE,redisInstanceId,pattern)));
		} catch (JedisConnectionException e) { 
			if (jedis != null)
				jedisPool.returnBrokenResource(jedis);
			logger.error("Cannot delete requested pattern due to connection pool error.", e);
			throw new RuntimeException("Unable to delete the requested pattern!");
		} catch (Exception e) {
			logger.error("Cannot delete requested key", e);
			throw new RuntimeException("Unable to delete the requested pattern!");
		} finally {
			releaseConnection(jedis);
		}
	}
	
}
