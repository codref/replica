/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import com.codref.r.replicaws.model.User;
import com.codref.r.replicaws.model.UserRoles;

@Repository
public class UserDAO extends BasicDAO {
	
	@SuppressWarnings("unchecked")
	public List<User> getUsersCount(String clauses) {
		return sessionFactory.getCurrentSession().createQuery("select count(*) from User "+clauses).list();
	}	

	@SuppressWarnings("unchecked")
	public List<User> getUsers() {
		return sessionFactory.getCurrentSession().createQuery("from User").list();
	}

	@SuppressWarnings("unchecked")
	public List<User> getUser(int id) {
		return sessionFactory.getCurrentSession().createQuery("from User where id = :id")
				.setParameter("id", id)
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getUsers(String clauses, int start, int limit) {
		return sessionFactory.getCurrentSession().createQuery("from User "+clauses).setFirstResult(start).setMaxResults(limit).list();
	}	

	public void deleteUser(int id){
		Object record = sessionFactory.getCurrentSession().load(User.class, id);
		sessionFactory.getCurrentSession().delete(record);
	}
	
	public User saveUser(User user){
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		return user;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserRoles> getRoles(int userId) {
		return sessionFactory.getCurrentSession().createQuery("from UserRoles where user_id = :userId")
				.setParameter("userId", userId).list();
	}
	
	public void saveRoles(UserRoles role){
		sessionFactory.getCurrentSession().saveOrUpdate(role);
	}	
	
	public void deleteRoles(int userId) {
		sessionFactory.getCurrentSession().createQuery("delete UserRoles where user_id = :userId")
				.setParameter("userId", userId).executeUpdate();
	}	
	
	public String getEncodedPassword(String username) {
		return (String) sessionFactory.getCurrentSession().createQuery("SELECT password FROM User WHERE username = :username AND enabled = 1")
				.setParameter("username", username).uniqueResult();
	}
}