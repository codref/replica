/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.codref.r.replicaws.model.RestSession;
import com.codref.r.replicaws.service.AuthenticationService;

@Controller
public class AuthenticationController {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private AuthenticationService authenticationService;
	
	@RequestMapping(value = "/authenticate.action")
	public @ResponseBody
	Map<String, Object> authenticateAction(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "password", required = true) String password) throws Exception {
		

		Map<String,Object> result = new HashMap<>();
		RestSession session = authenticationService.isAuthenticated(request);
		
		if (session != null)
			result.put("SpcRest-Auth-Token", session.getToken());	
		else {
			logger.info("Empty header or token not valid. Forced authentication through provided credentials");
			
			if (authenticationService.authenticateUser(username, password)) {
				logger.info("User " + username + " succesfully authenticated");
				// token generation
				session = authenticationService.createToken(); 			
				result.put("SpcRest-Auth-Token", session.getToken());						
			} else {
				logger.warn("User " + username + " attempted login with password " + password);
				return getModelMapError("Wrong credentials, the incident will be reported.");
			}
			
		}
		
		
		
		return getModelMap("authenticated", result);
	}
	
	private Map<String,Object> getModelMapError(String msg){

		Map<String,Object> modelMap = new HashMap<String,Object>(3);
		modelMap.put("message", msg);
		modelMap.put("success", false);

		return modelMap;
	} 		

	private Map<String,Object> getModelMap(String msg, Object data){

		Map<String,Object> modelMap = new HashMap<String,Object>(3);
		modelMap.put("message", msg);
		modelMap.put("data", data);
		modelMap.put("success", true);

		return modelMap;
	} 	
}
