/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codref.r.replicaws.dao.ConfigDAO;
import com.codref.r.replicaws.model.Config;
import com.codref.r.replicaws.util.GenericUtil;

@Service
public class ConfigService {

	@Autowired	
	private ConfigDAO configDAO;
	
	@Autowired
	private GenericUtil genericUtil;
	
	
	@Transactional
	public void delete(String data) throws JsonParseException, JsonMappingException, IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Integer id = genericUtil.getIdFromJSON(data, Config.class);
		configDAO.deleteConfig(id);
	}	

	@Transactional
	public Config update(String data) throws JsonParseException, JsonMappingException, IOException{
		Config config = genericUtil.getObjectFromJSON(data, Config.class);
		return configDAO.saveConfig(config);
	}	
	
	@Transactional
	public Config create(String data) throws JsonParseException, JsonMappingException, IOException{
		Config config = genericUtil.getObjectFromJSON(data, Config.class);
		return configDAO.saveConfig(config);
	}	
	
	@Transactional(readOnly=true)
	public long getConfigCount(String search) throws JsonParseException, JsonMappingException, IOException{
		String query = genericUtil.getWhere(search);
		List count = configDAO.getConfigsCount(query);
		Long countLong = (Long)count.get(0);
		return countLong;
	}	
	
	@Transactional(readOnly=true)
	public List<Config> getConfigList(Object page, Integer start, Integer limit, String search, String sort){
		try {
			int startInt = start;
			int limitInt = limit;
			String query = genericUtil.getWhereOrder(search, sort);
			return configDAO.getConfigs(query, startInt, limitInt);
		} catch (Exception e) {
			return configDAO.getConfigs();
		}
	}	

	/**
	 * Get all configs
	 * @return
	 */
	@Transactional(readOnly=true)
	public List<Config> getConfigList(){
		List<Config> Configs = configDAO.getConfigs();
		return Configs;
	}
	
	/**
	 * Get all configs with id = 
	 * @return
	 */
	@Transactional(readOnly=true)
	public String getConfigList(int id){
		List<Config> Configs = configDAO.getConfigs(id);
		Config config = Configs.get(0);
		if (config.getValue() == null){
			return config.getDefault_value();
		} else
			return config.getValue();
	}	
	
	/**
	 * Get all configs with label =
	 * @return
	 */
	@Transactional(readOnly=true)
	public String getConfigList(String label){
		Config config = configDAO.getConfigs(label).get(0);
		if (config.getValue() == null){
			return config.getDefault_value();
		} else
			return config.getValue();
	}
	
}
