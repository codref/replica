/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica               @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      --                    @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *      Web Service           @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *      Management Console    @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replicaws.setup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.codref.r.replicaws.model.Config;
import com.codref.r.replicaws.service.ConfigService;

@Controller
public class ConfigController extends BasicController {
	
	private static final String ERROR_TRYING_TO_DELETE_CONFIG_VALUE = "Error trying to delete config value.";

	private static final String ERROR_TRYING_TO_UPDATE_CONFIG_VALUE = "Error trying to update config value.";

	private static final String ERROR_TRYING_TO_CREATE_CONFIG_VALUE = "Error trying to create config value.";

	private static final String ERROR_RETRIEVING_CONFIG_VALUES_FROM_DATABASE = "Error retrieving config values from database.";

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private ConfigService configService;
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value="/setup/config/view.action")
	public @ResponseBody Map<String,? extends Object> view(
			@RequestParam(value = "page", required = false) Object page, 
			@RequestParam(value = "start", required = false) Integer start, 
			@RequestParam(value = "limit", required = false) Integer limit, 
			@RequestParam(value = "search", required = false) String search, 
			@RequestParam(value = "sort", required = false) String sort) throws Exception {
		
		try{

			List<Config> operators = configService.getConfigList(page, start, limit, search, sort);
			long count = configService.getConfigCount(search);
			return getMap(operators, count);

		} catch (Exception e) {
			logger.error(ERROR_RETRIEVING_CONFIG_VALUES_FROM_DATABASE,e);
			return getModelMapError(ERROR_RETRIEVING_CONFIG_VALUES_FROM_DATABASE);
		}
	}
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value="/setup/config/create.action")
	public @ResponseBody Map<String,? extends Object> create(@RequestParam String data) throws Exception {

		try{

			Config clients = configService.create(data);
			return getMap(clients);

		} catch (Exception e) {
			logger.error(ERROR_TRYING_TO_CREATE_CONFIG_VALUE,e);
			return getModelMapError(ERROR_TRYING_TO_CREATE_CONFIG_VALUE);
		}
	}
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value="/setup/config/update.action")
	public @ResponseBody Map<String,? extends Object> update(@RequestParam String data) throws Exception {
		try{

			Config configs = configService.update(data);
			return getMap(configs);

		} catch (Exception e) {
			logger.error(ERROR_TRYING_TO_UPDATE_CONFIG_VALUE,e);
			return getModelMapError(ERROR_TRYING_TO_UPDATE_CONFIG_VALUE);
		}
	}
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value="/setup/config/delete.action")
	public @ResponseBody Map<String,? extends Object> delete(@RequestParam String data) throws Exception {
		
		try{

			configService.delete(data);
			Map<String,Object> modelMap = new HashMap<String,Object>(3);
			modelMap.put("success", true);
			return modelMap;

		} catch (Exception e) {
			logger.error(ERROR_TRYING_TO_DELETE_CONFIG_VALUE,e);
			return getModelMapError(ERROR_TRYING_TO_DELETE_CONFIG_VALUE);
		}
	}
	
}