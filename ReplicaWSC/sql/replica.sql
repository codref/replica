-- MySQL dump 10.13  Distrib 5.5.37, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: replica
-- ------------------------------------------------------
-- Server version	5.5.37-0ubuntu0.13.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rs_config`
--

DROP TABLE IF EXISTS `rs_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rs_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_CONFIG_LABEL` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rs_config`
--

LOCK TABLES `rs_config` WRITE;
/*!40000 ALTER TABLE `rs_config` DISABLE KEYS */;
INSERT INTO `rs_config` VALUES (38,'resources.url',NULL,''),(40,'extjs.debug','true',NULL),(54,'instance.name',NULL,'replica');
/*!40000 ALTER TABLE `rs_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rs_output`
--

DROP TABLE IF EXISTS `rs_output`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rs_output` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `script` int(11) DEFAULT NULL,
  `variable` varchar(255) NOT NULL,
  `code` text,
  PRIMARY KEY (`id`),
  KEY `fk_rs_output_1` (`script`)
) ENGINE=MyISAM AUTO_INCREMENT=623 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rs_output`
--

LOCK TABLES `rs_output` WRITE;
/*!40000 ALTER TABLE `rs_output` DISABLE KEYS */;
INSERT INTO `rs_output` VALUES (622,1002,'image','paste0(t,\'.svg\')'),(616,1000,'normality','');
/*!40000 ALTER TABLE `rs_output` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rs_rest_session`
--

DROP TABLE IF EXISTS `rs_rest_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rs_rest_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `keepalive` datetime DEFAULT NULL,
  `token` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rs_rest_session`
--

LOCK TABLES `rs_rest_session` WRITE;
/*!40000 ALTER TABLE `rs_rest_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `rs_rest_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rs_script`
--

DROP TABLE IF EXISTS `rs_script`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rs_script` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` longtext,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rs_script`
--

LOCK TABLES `rs_script` WRITE;
/*!40000 ALTER TABLE `rs_script` DISABLE KEYS */;
INSERT INTO `rs_script` VALUES (1000,'normality','drv <- dbDriver(\'MySQL\')\nptm <- proc.time()\ncon <- dbConnect(drv, username=\'root\', host=\'localhost\', port=as.numeric(\'3306\'),\n                 dbname=\'test\', password=\'\')\nrs <- dbSendQuery(con, paste(\'SELECT value FROM rand_numbers \n  LEFT JOIN norm ON rand_numbers.number = norm.id \n  LIMIT\', start, \',\', limit));\ndata <- fetch(rs,n=-1)\nx <- data$value\nn <- ad.test(x)\nnormality <- if (n[2] >= 0.05) 1 else 0','distributed normality test'),(1002,'forecast','myts <- ts(v)\nfit <- ets(myts)\nt <- substring(tempfile(tmpdir=\'\'),2)\ntoSVG(filename=t)\nplot(forecast(fit, 30))\ncloseImage()','A \"forecast\" package sample script');
/*!40000 ALTER TABLE `rs_script` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rs_server`
--

DROP TABLE IF EXISTS `rs_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rs_server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `port` int(11) NOT NULL DEFAULT '6311',
  `weight` double DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rs_server`
--

LOCK TABLES `rs_server` WRITE;
/*!40000 ALTER TABLE `rs_server` DISABLE KEYS */;
INSERT INTO `rs_server` VALUES (5,'127.0.0.1',6311,1);
/*!40000 ALTER TABLE `rs_server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rs_user`
--

DROP TABLE IF EXISTS `rs_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rs_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(256) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rs_user`
--

LOCK TABLES `rs_user` WRITE;
/*!40000 ALTER TABLE `rs_user` DISABLE KEYS */;
INSERT INTO `rs_user` VALUES (1,'replicawsc','91c2e8519a30023f224f4c9341b09d636b92a83f459b90c3e171813fc81e6cfb21f1c6eef4ade6e7',1);
/*!40000 ALTER TABLE `rs_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rs_user_roles`
--

DROP TABLE IF EXISTS `rs_user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rs_user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `authority` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_roles` (`user_id`),
  CONSTRAINT `fk_user_roles` FOREIGN KEY (`user_id`) REFERENCES `rs_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rs_user_roles`
--

LOCK TABLES `rs_user_roles` WRITE;
/*!40000 ALTER TABLE `rs_user_roles` DISABLE KEYS */;
INSERT INTO `rs_user_roles` VALUES (23,1,'ROLE_USER'),(24,1,'ROLE_ADMIN'),(25,1,'ROLE_SETUP');
/*!40000 ALTER TABLE `rs_user_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-01 15:41:14
