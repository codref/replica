/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica Library       @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *                            @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *                            @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *                            @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replica;

public final class Constants {
	public static final String CACHE_SCRIPT_NAME_ID = "script.%s.id";
	public static final String CACHE_SCRIPT_NAME_CONTENT = "script.%s.content";
	
	public static final String CACHE_SCRIPT_ID_NAME = "script.%d.name";
	public static final String CACHE_SCRIPT_ID_CONTENT = "script.%d.content";
	
	public static final String CACHE_SCRIPT_OUTPUT_COUNT = "script.%d.output.count";
	public static final String CACHE_SCRIPT_OUTPUT_VARIABLE = "script.%d.output.%d.variable";
	public static final String CACHE_SCRIPT_OUTPUT_CODE = "script.%d.output.%d.code";
	
	public static final String CACHE_SERVER_COUNT = "server.count";
	public static final String CACHE_SERVER_ADDRESS = "server.%d.address";
	public static final String CACHE_SERVER_PORT = "server.%d.port";
	public static final String CACHE_SERVER_WEIGHT = "server.%d.weight";
	
	public static final String CACHE_KEY_INSTANCE_SESSION = "com.codref.r.replica.%s.%s.%s";
	public static final String CACHE_KEY_INSTANCE = "com.codref.r.replica.%s.%s";
	public static final String CACHE_NIL = "nil";
	
	public static final String ERROR_DATABASE_CONNECTION = "Database connection error";
	public static final String ERROR_RSERVE_CONNECTION = "Rserve connection error";
	public static final String ERROR_FETCHING_INNER = "Error while fetching %s. Inner Exception is: %s";
	public static final String ERROR_SERVER_CONNECTION = "Cannot connect to Rserve at %s:$d";
	public static final String ERROR_SCRIPT_NO_CONTENT_DEFINED = "Script %s has no content defined";
	public static final String ERROR_SCRIPT_NOT_DEFINED = "Script %s not present";
	public static final String ERROR_SCRIPT_FETCHING = "Error while fetching script %s. Inner Exception is: %s";	
	public static final String ERROR_SCRIPT_EVALUATION = "Error evaluating script of id %s. Inner exception is: %s";
	public static final String ERROR_OUTPUT_FETCH = "Error while fetching output values of script %s. Inner exception is: %s";
	public static final String ERROR_OUTPUT_EVALUATION = "Error evaluating output variable %s.  Inner Exception is: %s";
	public static final String ERROR_VARIABLE_ASSIGNATION = "Error, cannot assign variable %s";
	public static final String ERROR_CACHE_CONNECTION = "Cache service not available";
	
	public static final String R_EVAL_EXCEPTION = "try(eval(parse(text=.tmp.)),silent=TRUE)";
	
	public static final String SQL_SERVER_FETCH = "SELECT address, port, weight FROM %sserver";
	public static final String SQL_SCRIPT_FETCH = "SELECT name, content FROM %sscript WHERE id = ?";
	public static final String SQL_SCRIPT_FETCH_NAME = "SELECT id, content FROM %sscript WHERE name = ?";
	public static final String SQL_OUTPUT_FETCH = "SELECT variable, code FROM %soutput WHERE script = ?";
	
	public static final String FIELD_ADDRESS = "address";
	public static final String FIELD_PORT = "port";
	public static final String FIELD_WEIGHT = "weight";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_CONTENT = "content";
	public static final String FIELD_ID = "id";
	public static final String FIELD_VARIABLE = "variable";
	public static final String FIELD_CODE = "code";
}
