package com.codref.r.replica;

public class Output {
	private int script;
	private String variable;
	private String code;
	
	public Output() {}
	
	public Output(int scriptId, String variable, String code) {
		script  = scriptId;
		this.variable = variable;
		this.code = code;
	}
	
	public int getScript() {
		return script;
	}
	public void setScript(int script) {
		this.script = script;
	}
	public String getVariable() {
		return variable;
	}
	public void setVariable(String variable) {
		this.variable = variable;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
