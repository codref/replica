/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica Library       @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *                            @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *                            @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *                            @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replica;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.rosuda.REngine.*;
import org.rosuda.REngine.Rserve.*;

public class Replica {

	private String prefix;
	private Connection connection;
	private RConnection rconnection;
	
	private IReplicaCache cache;
	  
    /**
     * Create a new Replica object and connect it to the Replica database
     * 
     * @param connectionString The JDBC format connection string
     * @param driver The JDBC driver class
     * @param prefix Tables prefix used on the replica database
     * @param cacheHostAddress
     * @param cachePort
     * @param sessionId
     * @throws ReplicaException If database connection cannot be instantiated
     */
    
    public Replica(String connectionString, String driver, String prefix, IReplicaCache replicaCache) throws ReplicaException {
        this.prefix = prefix;
        try {
			Class.forName(driver);
			this.connection = DriverManager.getConnection (connectionString);
		} catch (ClassNotFoundException | SQLException e) {
			throw new ReplicaException(Constants.ERROR_DATABASE_CONNECTION, e);
		}
    	this.cache = replicaCache;
    }
    
    public IReplicaCache getCacheInstance() {
    	return cache;
    }
    
    /**
     * Connect Replica to the randomly selected Rserve.
     * 
     * @throws ReplicaException
     */
    
    public void connect() throws ReplicaException{
    	if (rconnection != null && rconnection.isConnected())
    		rconnection.close();
    	
    	ResultSet rs = null;
    	RandomCollection<Server> servers = new RandomCollection<>();
    	Server rserve;
    	
    	Integer cachedServersCount = cache.getServerCount();
    	if (cachedServersCount == null) {
	    	try {
	    		PreparedStatement stmt = connection.prepareStatement(String.format(Constants.SQL_SERVER_FETCH, this.prefix));
	    		rs = stmt.executeQuery();
	    		int index = 0;
	    		while (rs.next()) {
	    			String address = rs.getString(Constants.FIELD_ADDRESS);
	    			int port = rs.getInt(Constants.FIELD_PORT);
	    			double weight = rs.getDouble(Constants.FIELD_WEIGHT);
	    			Server server = new Server(address, port, weight);
	    			cache.setServer(index, server);
	    			servers.add(weight, server);
	    			index++;
	    		}
	    		cache.setServerCount(index);
	    	} catch (SQLException e) {
	    		throw new ReplicaException(String.format(Constants.ERROR_FETCHING_INNER, "server list", e.getMessage()), e);
	    	}
    	} else {
    		for (int index=0; index<cachedServersCount; index++) {
    			Server server = cache.getServer(index);
    			servers.add(server.getWeight(), server);    			
    		}
    	}

    	rserve = servers.next();
    	
    	try {
    		rconnection = new RConnection(rserve.getAddress(), rserve.getPort());
    	} catch (RserveException e) {
			throw new ReplicaException(String.format(Constants.ERROR_SERVER_CONNECTION,rserve.getAddress(), rserve.getPort()), e);
		}
    }
    
    /**
     * Close Rserve and database connections
     * 
     * @return True if correctly closed, false otherwise.
     * @throws SQLException If database connection cannot be closed
     */
    
    public boolean close() throws SQLException {
    	if (connection != null) {
    		connection.close();
    	}
    	if (rconnection != null)
    		return rconnection.close();
    	else
    		return false;
    }

    public HashMap<String,REXP> execute(int id) throws ReplicaException, SQLException {
    	String content;
		if ((content = cache.get(String.format(Constants.CACHE_SCRIPT_ID_CONTENT, id))) != null) {
			// retrieve script content from cache
			return execute(id, content);
    	} else {
    		// save script content to cache
    		Script script = getScriptContent(id);
    		return execute(id, script.getContent());
    	}
    }
    
    public HashMap<String,REXP> execute(String name) throws ReplicaException, SQLException {
    	Script script = cache.getScriptByName(name);
    	if (script == null) {
    		script = getScriptContent(name);
    		cache.setScript(script);
    	} 
    	return execute(script.getId(), script.getContent());

    }    
    
    private Script getScriptContent(int id) throws ReplicaException, SQLException {
        ResultSet rs = null;
    	PreparedStatement stmt;
    	try {
    		stmt = connection.prepareStatement(String.format(Constants.SQL_SCRIPT_FETCH, prefix));
    		stmt.setInt(1, id);
    		rs = stmt.executeQuery();
        	if (rs.next()) {
        	   	if (rs.getString(Constants.FIELD_CONTENT).length() > 0) {
        	   		return new Script(id, rs.getString(Constants.FIELD_NAME),  rs.getString(Constants.FIELD_CONTENT));
        	   	} else
        	    	throw new ReplicaException(String.format(Constants.ERROR_SCRIPT_NO_CONTENT_DEFINED, Integer.toString(id)));
        	} else
        	   	throw new ReplicaException(String.format(Constants.ERROR_SCRIPT_NOT_DEFINED, Integer.toString(id)));
    	} catch (SQLException e) {
    		throw new ReplicaException(String.format(Constants.ERROR_SCRIPT_FETCHING, Integer.toString(id), e.getMessage()), e);
    	} finally {
    		if (rs != null)
    			rs.close();
    	}
    }
    
    private Script getScriptContent(String name) throws ReplicaException, SQLException {
        ResultSet rs = null;
    	PreparedStatement stmt;    	
    	try {
    		stmt = connection.prepareStatement(String.format(Constants.SQL_SCRIPT_FETCH_NAME, prefix));
    		stmt.setString(1, name);
    		rs = stmt.executeQuery();
        	if (rs.next()) {
        	   	if (rs.getString(Constants.FIELD_CONTENT).length() > 0) {
        	   		return new Script(rs.getInt(Constants.FIELD_ID), name, rs.getString(Constants.FIELD_CONTENT));
        	   	} else
        	    	throw new ReplicaException(String.format(Constants.ERROR_SCRIPT_NO_CONTENT_DEFINED, name));
        	} else
        	   	throw new ReplicaException(String.format(Constants.ERROR_SCRIPT_NOT_DEFINED, name));
    	} catch (SQLException e) {
    		throw new ReplicaException(String.format(Constants.ERROR_SCRIPT_FETCHING, name, e.getMessage()), e);
    	} finally {
    		if (rs != null)
    			rs.close();
    	} 	
    }
    
    private HashMap<String,REXP> execute(int scriptId, String scriptContent) throws ReplicaException, SQLException {
    	
    	HashMap<String,REXP> result = new HashMap<String,REXP>(); 
    	
    	try {
    		rconnection.voidEval(scriptContent);
    	} catch (RserveException e) {
    		String rmessage = "";
    		try {
    			rmessage = getErrorMessage(scriptContent);
    		} catch (REngineException | REXPMismatchException e1) {
    			throw new ReplicaException(String.format(Constants.ERROR_SCRIPT_EVALUATION, Integer.toString(scriptId), e.getMessage()), e);
    		}
    		throw new ReplicaException(String.format(Constants.ERROR_SCRIPT_EVALUATION, Integer.toString(scriptId), rmessage), e);
    	}
    	
    	result = getOutputScript(scriptId);
    	return result;
    }
    
    private HashMap<String,REXP> getOutputScript(int script) throws ReplicaException, SQLException {
    
    	// TODO transform output hash map to something more elegant (using output class) 
    	
    	if (!rconnection.isConnected())
    		throw new ReplicaException(Constants.ERROR_RSERVE_CONNECTION);    	
    	
    	HashMap<String,REXP> result = new HashMap<String,REXP>();
    	HashMap<String, String> outputMap = new HashMap<>();
    	
    	Integer cachedOutputCount = cache.getOutputCount(script);
    	if (cachedOutputCount == null) {
        	ResultSet rs = null;
    		PreparedStatement stmt;
    		try {
    			stmt = connection.prepareStatement(String.format(Constants.SQL_OUTPUT_FETCH, prefix));
    			stmt.setInt(1, script);
    			rs = stmt.executeQuery();
    			int index = 0;
    			while (rs.next()) {
    				String variable = rs.getString(Constants.FIELD_VARIABLE).trim();
    				String code = rs.getString(Constants.FIELD_CODE);
    				if (rs.wasNull() || code.equals("")) {
    					outputMap.put(variable, null);
    					cache.setOutput(index, new Output(script, variable, ""));
    				} else {
    					outputMap.put(variable, code);
    					cache.setOutput(index, new Output(script, variable, code));
    				}
    				index++;
        	    }
    			cache.setOutputCount(script, index);
    		} catch (SQLException e) {
    			throw new ReplicaException(String.format(Constants.ERROR_OUTPUT_FETCH, script, e.getMessage()), e);
    		} finally {
    			if (rs != null)
    				rs.close();
    		}
    	} else {
    		for(int index=0; index < cachedOutputCount; index++) {
    			Output output = cache.getOutput(script, index);
    			outputMap.put(output.getVariable(), output.getCode().equals("") ? null : output.getCode());
    		}
    	}


    	for (Map.Entry<String, String> entry : outputMap.entrySet()) { 
    		String variable = entry.getKey();
    		String code = entry.getValue();
    		try {
    			if (code == null) {
    				result.put(variable, rconnection.get(variable, null, true));
    			} else {
    				try {
    					result.put(variable, rconnection.eval(code));
    				} catch (REngineException e) {
    					String rmessage = "";
    					try {
    						rmessage = getErrorMessage(code);
    					} catch (REngineException | REXPMismatchException e1) {
    						throw new ReplicaException(String.format(Constants.ERROR_OUTPUT_EVALUATION, variable, e.getMessage()), e);
    					}
    					throw new ReplicaException(String.format(Constants.ERROR_OUTPUT_EVALUATION, variable, rmessage), e);
    				}
    			}
    		} catch (REngineException e) {
    			throw new ReplicaException(String.format(Constants.ERROR_OUTPUT_EVALUATION, variable, e.getMessage()), e);					
    		}

    	}
    	return result;
    }
    
    private String getErrorMessage(String script) throws REngineException, REXPMismatchException, ReplicaException {
    	if (!rconnection.isConnected())
    		this.connect();
    	rconnection.assign(".tmp.", script);
    	REXP r = rconnection.parseAndEval(Constants.R_EVAL_EXCEPTION);
    	if (r.inherits("try-error")) { 
    		return r.asString();
    	} else
    		return "NULL";
    }
    
    public void assignParameter(String symbol, Float d) throws ReplicaException, RserveException {
    	if (d == null) {
    		rconnection.assign(symbol, "");
    	} else {
	    	try {
				rconnection.assign(symbol, new REXPDouble(d), null);
			} catch (REngineException e) {
				throw new ReplicaException(String.format(Constants.ERROR_VARIABLE_ASSIGNATION, symbol), e);
			}
    	}
    }  
    
    public void assignParameter(String symbol, double d) throws ReplicaException {
    	try {
			rconnection.assign(symbol, new REXPDouble(d), null);
		} catch (REngineException e) {
			throw new ReplicaException(String.format(Constants.ERROR_VARIABLE_ASSIGNATION, symbol), e);
		}
    }    
    
    public void assignParameter(String symbol, double[] d) throws ReplicaException {
    	try {
			rconnection.assign(symbol, d);
		} catch (REngineException e) {
			throw new ReplicaException(String.format(Constants.ERROR_VARIABLE_ASSIGNATION, symbol), e);
		}
    }
    
    public void assignParameter(String symbol, int[] d) throws ReplicaException {
    	try {
			rconnection.assign(symbol, d);
		} catch (REngineException e) {
			throw new ReplicaException(String.format(Constants.ERROR_VARIABLE_ASSIGNATION, symbol), e);
		}
    }
    
    public void assignParameter(String symbol, String[] d) throws ReplicaException {
    	try {
			rconnection.assign(symbol, d);
		} catch (REngineException e) {
			throw new ReplicaException(String.format(Constants.ERROR_VARIABLE_ASSIGNATION, symbol), e);
		}    	
    }
    
    public void assignParameter(String symbol, byte[] d) throws ReplicaException {
    	try {
			rconnection.assign(symbol, d);
		} catch (REngineException e) {
			throw new ReplicaException(String.format(Constants.ERROR_VARIABLE_ASSIGNATION, symbol), e);
		}    	
    }
    
    public void assignParameter(String symbol, String d) throws ReplicaException {
    	try {
			rconnection.assign(symbol, d);
		} catch (REngineException e) {
			throw new ReplicaException(String.format(Constants.ERROR_VARIABLE_ASSIGNATION, symbol), e);
		}    	
    }
    
    public void assignParameter(String symbol, REXP value) throws ReplicaException {
    	try {
			rconnection.assign(symbol, value);
		} catch (REngineException e) {
			throw new ReplicaException(String.format(Constants.ERROR_VARIABLE_ASSIGNATION, symbol), e);
		}
    }
}
