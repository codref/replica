/*
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@      '    
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@           @@@ @@@@@@@@@@@@ @@@@@@@@@@@@    @@@@@@@@@@  ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@          ''  '  ''
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *                            @@           @@@       @@           @@@ @@           @@@       @@    @@            '''''  
 *      Replica Library       @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *                            @@           @@@       @@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@  @@@@@@@       ''  '  ''
 *                            @@           @@@       @@  @@       @@@ @@           @@@             @@              '   
 *                            @@           @@@       @@  @@       @@@ @@           @@@             @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                            @@@@@@@@@@@@ @@@@@@@@@@@@  @@@@@@@@@@@@ @@           @@@@@@@@@@@@    @@                   
 *                                                                                                                     
 * 
 *
 * Copyright (c) 2014 Davide Dal Farra, dalfarra@codref.com
 *
 * Licensed under the License specified in file LICENSE, included with
 * the source code and binary code bundles.
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codref.r.replica;

import redis.clients.jedis.Jedis;

public class ReplicaCache implements IReplicaCache {	
	
	private Jedis jedis;
	private String host;
	private int port;
	private String sessionId;
	private String instanceId;
	
	public ReplicaCache(Jedis jedis, String instanceId, String sessionId) throws ReplicaException {
		this.jedis = jedis;
		this.sessionId = sessionId;
		this.instanceId = instanceId;
		
		if (jedis == null) 
			throw new ReplicaException(Constants.ERROR_CACHE_CONNECTION);
 		
    	if (!this.jedis.isConnected())
    		try {
    			jedis.connect();
    		} catch (Exception e) {
    			throw new ReplicaException(Constants.ERROR_CACHE_CONNECTION);
    		}
	}
	
	public ReplicaCache(String host, int port, String instanceId) throws ReplicaException {
		this(host, port, instanceId, "");
	}
	
	public ReplicaCache(Jedis jedis, String instanceId) throws ReplicaException {
		this(jedis, instanceId, "");
	}	
	
	public Jedis getJedisInstance() {
		return jedis;
	}
	
	// TODO better define constructor, include try catch
	public ReplicaCache(String host, int port, String instanceId, String sessionId) throws ReplicaException {
		this(new Jedis(host, port), instanceId, sessionId);
		this.host = host;
		this.port = port;		
	}

	@Override
	public String getSessionWide(String key) {
		String result = jedis.get(String.format(Constants.CACHE_KEY_INSTANCE_SESSION, instanceId, sessionId, key));
		return result == null || result.equals(Constants.CACHE_NIL) ? null : result;
	}
	@Override
	public String get(String key) {
		String result = jedis.get(String.format(Constants.CACHE_KEY_INSTANCE,instanceId,key));
		return result == null || result.equals(Constants.CACHE_NIL) ? null : result;
	}
	
	@Override
	public String setSessionWide(String key, String value) {
		return jedis.set(String.format(Constants.CACHE_KEY_INSTANCE_SESSION, instanceId, sessionId, key), value);
	}	
	
	@Override
	public String set(String key, String value) {
		return jedis.set(String.format(Constants.CACHE_KEY_INSTANCE,instanceId,key), value);
	}
	
	/* (non-Javadoc)
	 * @see com.codref.r.replica.ICache#isConnected()
	 */
	@Override
	public boolean isConnected() {
		return jedis.isConnected();
	}
	
	public String getCacheHostAddress() {
		return host;
	}
	public void setCacheHostAddress(String host) {
		this.host = host;
	}
	public int getCachePort() {
		return port;
	}
	public void setCachePort(int port) {
		this.port = port;
	}
	/* (non-Javadoc)
	 * @see com.codref.r.replica.ICache#getSessionId()
	 */
	@Override
	public String getSessionId() {
		return sessionId;
	}
	/* (non-Javadoc)
	 * @see com.codref.r.replica.ICache#setSessionId(java.lang.String)
	 */
	@Override
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	@Override
	public Integer getServerCount() {
		String count = get(Constants.CACHE_SERVER_COUNT);
		if (count == null || count.equals(""))  return null; else  return Integer.parseInt(count);
	}

	@Override
	public void setServerCount(int count) {
		set(Constants.CACHE_SERVER_COUNT, Integer.toString(count));
	}

	@Override
	public Server getServer(int index) {
		return new Server(
				get(String.format(Constants.CACHE_SERVER_ADDRESS, index)),
				Integer.parseInt(get(String.format(Constants.CACHE_SERVER_PORT, index))),
				Double.parseDouble(get(String.format(Constants.CACHE_SERVER_WEIGHT, index)))
		);
	}

	@Override
	public void setServer(int index, Server server) {
		set(String.format(Constants.CACHE_SERVER_ADDRESS, index), server.getAddress());
		set(String.format(Constants.CACHE_SERVER_PORT, index), Integer.toString(server.getPort()));
		set(String.format(Constants.CACHE_SERVER_WEIGHT, index), Double.toString(server.getWeight()));
	}

	@Override
	public Script getScriptByName(String name) {
		String id = get(String.format(Constants.CACHE_SCRIPT_NAME_ID, name));
		if (id == null || id.equals(""))
			return null; 
		else
			return new Script(Integer.parseInt(id), name, get(String.format(Constants.CACHE_SCRIPT_NAME_CONTENT, name)));
	}

	@Override
	public void setScript(Script script) {
		set(String.format(Constants.CACHE_SCRIPT_NAME_ID, script.getName()), script.getId().toString());
		set(String.format(Constants.CACHE_SCRIPT_NAME_CONTENT, script.getName()), script.getContent());
	}

	@Override
	public Integer getOutputCount(int scriptId) {
		String count = get(String.format(Constants.CACHE_SCRIPT_OUTPUT_COUNT, scriptId));
		if (count == null || count.equals(""))  return null; else  return Integer.parseInt(count);
	}

	@Override
	public void setOutputCount(int scriptId, int count) {
		set(String.format(Constants.CACHE_SCRIPT_OUTPUT_COUNT, scriptId), Integer.toString(count));	
	}

	@Override
	public Output getOutput(int scriptId, int index) {
		return new Output(
				scriptId, 
				get(String.format(Constants.CACHE_SCRIPT_OUTPUT_VARIABLE, scriptId, index)),
				get(String.format(Constants.CACHE_SCRIPT_OUTPUT_CODE, scriptId, index))
		);
	}

	@Override
	public void setOutput(int index, Output output) {
		set(String.format(Constants.CACHE_SCRIPT_OUTPUT_VARIABLE, output.getScript(), index), output.getVariable());
		set(String.format(Constants.CACHE_SCRIPT_OUTPUT_CODE, output.getScript(), index), output.getCode());		
	}
}
